---
title: Télédétection - TD4
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer2.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/image_zoom.less"
@import "less/iframe.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/blocks.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection  </section></center>
<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD 4 :  Calcul d'indices spectraux - classification par seuillage</section></center>
<br>
<center><section style="font-size:150%"> Marc Lang </section></center>
<p> </p>

<iframe src="https://remonterletemps.ign.fr/comparer/basic?x=1.426293&y=43.607926&z=13&layer1=ORTHOIMAGERY.ORTHOPHOTOS.1950-1965&layer2=ORTHOIMAGERY.ORTHOPHOTOS&mode=vSlider" scrolling="yes" width="100%" height="700px" frameborder="0"></iframe>

<div class="question">
	<label> Curiosité n°4 : D'après vous, de quoi s'agit-il ?
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Cette animation est issue du site [remonter dans le temps](https://remonterletemps.ign.fr) qui permet de visualiser les anciennes photographies aériennes.

  Vous pouvez voir ici une comparaison entre les photos aériennes de 1950-1965 et les photos actuelles autour de l'aglomération de Toulouse. Vous pouvez voir de manière flagrante l'évolution de l'aire urbaine et l'évolution du parcellaire agricole (des parcelles beaucoup plus grandes aujourd'hui).

</span>
</label>
<br><br>
</div>

<section style="font-size:200%"> Contenu </section></center>


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=4 orderedList=False} -->
<!-- code_chunk_output -->

  - [Les données](#les-données)
  - [Principe](#principe)
  - [Application](#application)
- [Ressources Annexes](#ressources-annexes)

<!-- /code_chunk_output -->

## Les données

Vous pouvez télécharger les données du TD --> [ici](https://filesender.renater.fr/?s=download&token=d2c671b7-4d3c-4ffd-afe1-afa743e38f96) <--.

<div class="block green-block"><p class="title exercice"></p>

1. Téléchargez les données ;

</div>

## Principe

La détection des changements en télédétection consiste à détecter des différences entre deux images, ou un lot d'images. Elle peut être utilisée pour :

- détecter des changements de propriétés de la végétation (ex : maladies) ;
- l'occupation du sol (ex : urbanisation) ;
- l'étude des dégats de catastrophes naturelles.

Dans ce TD, nous allons essayer de détecter les zones inondées suite au Tsunami de 2004 dans l'océan indien. Des images du satellite [QuickBird](https://www.euspaceimaging.com/about/satellites/quickbird/) ont été prises sur le site de Lhonga Leupung avant et après le tsunami :


<figure>
	<img src="img/tsunami_before.jpg" title="Site de Lhonga Leupung en Infra rouge Couleurs avant le tsunami" width="45%"><img src="img/tsunami_after.jpg" title="Site de Lhonga Leupung en Infra rouge Couleurs après le tsunami" width="45%">
	<figcaption>Site de Lhonga Leupung en Infra rouge Couleurs avant (à gauche) et après (à droite) le tsunami.</figcaption>
</figure>


## Application

Vous allez travailler en groupe pour essayer de produire une carte des inondations. Vous n'avez pas besoin d'autres outils que ceux vus précédemment pour produire la carte et vous avez accès aux précédents TDs. Je ne vous donnerai donc pas d'indications techniques pour commencer.

Il n'y a pas de solution unique pour traiter ce problème et il est possible que chaque groupe propose une démarche différente !

Quelques recommandations cependant pour commencer :
- prenez le temps de décortiquer le problème, de vous remémorer les outils et les concepts étudiés plus tôt.
- établissez un plan d'action :
  - qu'allez vous tester/regarder en premier ?
  - que faites vous ensuite si ça marche ? si ça ne marche pas ?
  - faîtes rapidement un schéma général de ce que vous prévoyez de faire
- répartissez vous les taches équitablement
- vous avez le temps, n'hésitez pas à tester

<center><div style="font-size:200%"> À vous de jouer ! </div></center>

<br>
<br>


---------
# Ressources Annexes

- [Aide sur l'utilisation de l'OTB](s8_installation.html#utilisation-de-lotb)
- [Aide sur l'utilisation d'un terminal](aide_lcmd.html)
