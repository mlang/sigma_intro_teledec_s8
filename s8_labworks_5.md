---
title: Télédétection - TD5
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer2.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/image_zoom.less"
@import "less/iframe.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/blocks.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection  </section></center>
<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD5 - Classification supervisée</section></center>
<br>
<center><section style="font-size:150%"> Marc Lang </section></center>
<p> </p>

<iframe src="http://greencitylab.terranis.fr/webmap/vegClassifTlsePleiades/index.html#16/43.5943/1.4508" scrolling="yes" width="100%" height="700px" frameborder="0"></iframe>

<div class="question">
	<label> Curiosité n°5 : D'après vous, de quoi s'agit-il ?
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Cette animation est issue du site de l'entreprise [TerraNIS](http://terranis.fr/) qui fournit des services de géoinformation basés sur
  l'Observation de la Terre sur des thématiques d'agriculture, Viticulture et d'environnement.
  <br> Vous visualisez ici le Green City Lab de TerraNIS qui est une "sélection de produits qui permettent de prendre conscience de la place occupée par la nature dans les villes. La plupart de ces données et indicateurs ont été développés dans le cadre du projet Nature4Cities et seront donc évalués par des professionnels des secteurs universitaire, public et privé."

</span>
</label>
<br><br>
</div>

<section style="font-size:200%"> Contenu </section></center>


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=4 orderedList=False} -->
<!-- code_chunk_output -->

  - [Les données](#les-données)
  - [Principe](#principe)
    - [Echantillons d'apprentissage](#echantillons-dapprentissage)
    - [Algorithmes](#algorithmes)
  - [Chaîne de traitement](#chaîne-de-traitement)
  - [Post classification](#post-classification)
  - [Automatisation du processus](#automatisation-du-processus)
  - [Pour aller plus loin](#pour-aller-plus-loin)
    - [Mais qui m'a numérisé cette classe Bâti ?!](#mais-qui-ma-numérisé-cette-classe-bâti)
    - [L'hiver, les arbres perdent leurs feuilles ...](#lhiver-les-arbres-perdent-leurs-feuilles)
- [Ressources Annexes](#ressources-annexes)

<!-- /code_chunk_output -->

## Les données

Vous pouvez télécharger les données du TD --> [ici](https://filesender.renater.fr/?s=download&token=add8a560-5a79-4d05-b445-f6797afe47b9) <--

<div class="block green-block"><p class="title exercice"></p>

1. Téléchargez les données ;

</div>
 

## Principe

L'objectif de cette partie est d'effectuer la classification d'images de télédétection de manière **supervisée**. Le principe reste le même que celui du seuillage que vous avez fait précédemment : il s'agit d'attribuer un label aux pixels d'une image en fonction de leur comportement radiométrique. Avec le seuillage, l'attribution des labels s'est faite manuellement en délimitant des zones sur les histogrammes 1D ou les scatterplots 2D, l'attribution s'est faite de manière **experte**, avec des règles de décision que **vous** avez déterminées.

Avec les algorithmes de classification supervisée, les règles de décision permettant d'attribuer un label à une classe sont calculées automatiquement à partir d'**échantillons d'apprentissage**. Manuellement, vous avez vu qu'il est difficile d'utiliser l'information de plus de deux bandes. C'est ici beaucoup plus simple. Dans ce TD, vous allez classifier les deux images Fabas. D'abord séparemment, puis conjointement.

### Echantillons d'apprentissage

Pour entraîner un algortihme de classification supervisée, il faut disposer d'échantillons d'apprentissage, c'est à dire des pixels dont on connait *a priori* la nature, des pixels déjà labelisés. Il est possible de construire cette vérité terrain (ensemble de pixels labelisés) de différentes manières :

- en utilisant une source de données exogènes (BD TOPO, RPG, CLC) et en extrayant les informations pertinentes au niveau du pixel ;
- en réalisant des rélevés terrain géoréférencés ;
- en faisant de la photo-interprétation (si possible).

Pour ce TD, les échantillons de vérité terrain sont fournis sous forme de fichier vecteur :

<figure>
	<img src="img/label_fabas.jpg" title="Vérité Terrain pour le site Fabas" width="100%">
	<figcaption>Vérité Terrain pour le site Fabas.</figcaption>
</figure>



Cinq classes sont considérées :


<div class="my_table">


|        Classe         | Attribut |
|:---------------------:|:--------:|
| Végétation clairsemée |    1     |
|        Sol Nu         |    2     |
|  Végétation ligneuse  |    3     |
|          Eau          |    4     |
|         Bâti          |    5     |

<center><div class="table-caption">Classes des échantillons.</div></center></div>

### Algorithmes

Plusieurs algorithmes de classification supervisée sont disponibles dans l'OTB, chacun possédant ses propres caractérisitiques. La compréhension détaillée du fonctionnement de ces algortihmes va bien au délà de la portée de ce TD. Il est cependant utile de comparer leur résultats en termes de précision et de temps de calcul.

Dans ce TD, vous allez comparer différents algorithmes d'une part :

  -  K-nn
  -  Bayes;
  -  SVM ;
  -  Random-Forest

et différents jeux d'apprentissage d'autre part :

- la vérité terrain pour une date ;
- la vérité terrain pour deux dates _concaténées_.

## Chaîne de traitement

La classification d'une image se fait en plusieurs étapes :

- l'**apprentissage du modèle**, à l'aide de l'application OTB [==TrainImagesClassifier==](https://www.orfeo-toolbox.org/CookBook/Applications/app_TrainImagesClassifier.html). L'apprentissage se fait à l'aide des images à classer et des échantillons d'apprentissage. Il consiste à déterminer **les règles de décisions** qui permettront d'assigner un label à un pixel. Après cette étape, les règles de décisions (ou paramètres du modèle) sont enregistrées dans un fichier texte.

- la **classification de l'image** à l'aide de l'application OTB [==ImageClassifier==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ImageClassifier.html). Cette étape consiste à appliquer le modèle précédemment paramétré sur l'ensemble des pixels de l'image.

- l'**évaluation de la qualité** de la classification à l'aide de l'application OTB [==ComputeConfusionMatrix==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ComputeConfusionMatrix.html). Cette étape nécessite l'utilisation de vérités terrain de validation qui n'aient pas été utilisées pour l'apprentissage. À l'issue de cette étape, la matrice de confusion est enregistrée dans un fichier texte.

L'ensemble de la chaîne de traitements vous est résumée ci-dessous :

<figure>
	<img src="img/classif_workflow.png" title="Chaîne de traitement pour la classification d'une image. En bleu, les données d'entrée; en rouge, les applications OTB; en vert les sorties des applications." width="90%">
	<figcaption> Chaîne de traitements de classification.</figcaption>
</figure>



<div class="block blue-block">
<p class="title"> Astuce n°1</p>

Pour les plus curieu.ses.x, il existe d'autres chaînes de traitements possibles pour la classification d'une image de télédétection, notamment sur la manière de répartir les échantillons en un jeu d'entrainement et de validation. Vous pourrez trouver plus de détails [ICI](https://www.orfeo-toolbox.org/CookBook/recipes/pbclassif.html).
</div>

<div class="block blue-block">
<p class="title"> Astuce n°2</p>

L'étape compute [==ComputeImageStatistics==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ComputeImagesStatistics.html) n'est nécessaire que pour certains algorithmes, notamment l'algorithme SVM. Elle permet de fournir des informations sur la distribution des bandes  de l'image à classer (moyenne, écart-type) à l'algorithme.
</div>

<div class="block green-block"><p class="title exercice"></p>

Pour une image, par ex: *fabas_12_10_2013.tif*, et un algorithme seulement, par ex: *K-nn* :

- **1.** Apprendre le modèle
- **2.** Classer l'image
- **3.** Analyser de la matrice de confusion
  - **3.1.** Produire la matrice de confusion et l'enregistrer
  - **3.2.** Ouvrir la matrice de confusion dans un tableur (Excel/ Tableur libre office) et produire les indices de qualité suivants :
    - l'accord global ;
    - la précision de chaque classe (ou précision utilisateur ou erreur de commision) ;
    - le rappel de chaque classe (ou précision réalisateur ou erreur d'omission) ;
  - la qualité globale de la classification est-elle bonne ?
  - quelles classes sont mal classées ? Pourquoi ?
  - quelles sont les plus grosses confusions ?

</div>

<div class="block blue-block">
<p class="title"> Astuce n°3</p>

Vous pourrez également trouver les indices de qualité **dans les logs de l'application OTB** [==ComputeConfusionMatrix==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ComputeConfusionMatrix.html). Vous pourrez ainsi vérifier vos calculs.

</div>

Vous pouvez remarquer à ce stade là qu'il est possible, en théorie, d'analyser la qualité d'une image de classification sans même l'ouvrir. C'est par contre une très mauvaise pratique. Il est toujours recommander de compléter l'analyse statistique de la qualité par une analyse visuelle. Pour ce faire :


<div class="block green-block"><p class="title exercice"></p>

- **4.** Améliorez le rendu visuelle de l'image classée
    - **4.1.** soit manuellement en changeant le mode de représentation de l'image ([==Palette /Uniques==](https://docs.qgis.org/3.10/fr/docs/user_manual/working_with_raster/raster_properties.html#symbology-properties))
    - **4.2** soit automatiquement en utilisant l'application OTB [==ColorMapping==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ColorMapping.html?highlight=otbcli_colormapping)
- **5.** Repérez dans l'image classée les erreurs de commision et d'omission que vous avez relevées dans la matrice de confusion.

</div>

<div class="block blue-block">
<p class="title"> Rappel</p>

**Matrice de confusion**


<center><img src="img/Matrice_conf_theorique.png" title="Matrice de confusion théorique" width="50%"></center>

**Accord Globale**

C'est le pourcentage de pixels bien classés, ce qui correspond à la somme de la diagonale de la matrice de confusion:

$$OA = \frac{\sum_i n_{ii}}{n}$$



**Rappel**

C'est le pourcentage des pixels de référence d'une classe *i* effectivement classés dans la classe *i* par l’algorithme. Cela correspond aux pixels bien classées de la classe *i* divisé (dans la digonale) par la somme des pixels de la ligne correspondante :

$$Recall_i = \frac{n_{ii}}{\sum_j n_{ij}}$$

**Précision**

C'est le pourcentage des pixels classés comme une classe i par l'algorithme qui correspondent effectivement à cette classe d'après la référence. Cela correspond aux pixels bien classées de la classe i (dans la digonale) divisé par la somme des pixels de la colonne correspondante :

$$Precision_i = \frac{n_{ii}}{\sum_i n_{ij}}$$

</div>

## Post classification

Il est possible de réaliser des traitements post-classification sur image déjà classifiée. Il est par exemple possible d'appliquer des filtres sur une image pour supprimer un effet poivre et sel.

<figure>
	<img src="img/markdown-img-paste-20210224104548593.png" title="" width="49%">
<img src="img/markdown-img-paste-20210224104522201.png" title="" width="49%">
	<figcaption>Exemple du passage d'un filtre majoritaire 3x3.</figcaption>
</figure>

Vous pouvez appliquer un filtre à l'aide de l'application [==ClassificationMapRegularization==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ClassificationMapRegularization.html)

<div class="block green-block"><p class="title exercice"></p>

1. Appliquez le filtre sur votre classification avec un filtre 3 x 3 ;
2. Vérifiez que l'opération a bien fonctionné en visualisant le résultat ;
3. Calculez la qualité de cette nouvelle classification.
4. Testez d'autres tailles de filtre en observant :
   - de manière visuelle l'effet du filtre;
   - l'impact sur l'estimation de la qualité de la classification.

</div>




## Automatisation du processus

L'objectif ici est de tester différents algorithmes et différents jeux de données. Répéter la chaîne de traitements pas à pas peut être long et fastidieux. Vous allez créer une chaîne de traitement automatique.

<div class="block green-block"><p class="title exercice"></p>

- **1**. Automatisez la chaine :
  - Soit avec l'aide [==Graphical modeler==](https://docs.qgis.org/3.10/fr/docs/user_manual/processing/modeler.html) ;
  - Soit en lignes de commandes successives à éxecuter dans un ==Terminal== ;

</div>

Une fois l'automatisation effectuée, vous allez pouvoir comparer différentes sources de données et différents algorithmes de classification très rapidement.

<div class="block green-block"><p class="title exercice"></p>


- **2**. Comparez les performances de classifications issues :
  - de `fabas_12_10_2013.tif`, du NDVI de cette même date et de la  concaténation de `fabas_12_10_2013.tif` et du NDVI ;
  - l'image `fabas_10_12_2013.tif`, l'image `fabas_12_10_2013.tif` et la concatenation des deux images.
  -  qu'en concluez vous ?

- **3**. Comparez les performances des différents classifieurs sur l'image de votre choix.

</div>

<iframe src="https://lite.framacalc.org/9lyh-s8_teledetection_classif" height="700" frameborder=0 ></iframe>

<br>
<br>

## Pour aller plus loin

### Mais qui m'a numérisé cette classe Bâti ?!

Vous avez repéré qu'il y avait des problèmes avec la classe *Bâti*. Vous pouvez regarder le comportement spectal d'échantillons à l'aide de ==RasterDataPlotting== à l'aide de l'option ROI  : ![](img/markdown-img-paste-20210224162255813.png).

<figure>
	<img src="img/markdown-img-paste-20210224162704216.png" title="" width="80%">
	<figcaption>Scatter plot 2D des échantillons de validation pour l'image d'Octobre (X : rouge, Y : infra rouge). Bleu : Eau, Jaune : Sol, Rouge : Bâti, Vert Pomme : Végétation Clairsemée, Vert foncé : Végétation Ligneuse  </figcaption>
</figure>


<div class="block green-block"><p class="title exercice"></p>

1. Observez le comportement spectrale de la classe *bâti* de la couche `valid_fabas_l93.shp` sur un scatter plot 2D de l'image fabas d'octobre `fabas_12_10_13.tif` avec la bande Rouge en X et la bande IR en Y ;
2. Que constatez vous ?
3. Copiez les couches `valid_fabas_l93.shp` et `train_fabas_l93.shp` et renommez les `new_valid_fabas_l93.shp` et `new_train_fabas_l93.shp` ;
4. Supprimez les polygones *bâti* de ces deux nouvelles couches ;
5. Ajoutez au moins dix polygones dans chaque couche ;
6. Refaites une classification de type *Knn* ;
7. Appliquez un filtre majoritaire de taille 3x3 ;
8. Évaluez la qualité de la carte obtenue ;

</div>

### L'hiver, les arbres perdent leurs feuilles ...

Vous avez également pu constater qu'il y avait une grosse baisse de précision pour la classe *Végétation ligneuse* lorsqu'on fait la classification sur la date de Décembre. Essayons de remédier à ça.

<div class="block green-block"><p class="title exercice"></p>

**Identifier le problème**
1. Observez le comportement spectral de la classe *Végétation ligneuse* de la couche `valid_fabas_l93.shp` sur un scatter plot 2D de l'image fabas de Décembre `fabas_10_12_13.tif` avec la bande Rouge en X et la bande IR en Y ;
2. Que constatez vous ?
3. À quoi correspondent les deux *modes* que vous avez repérés selon vous ?


</div>

En hiver, le comportement spectral de classe végétation ligneuse devient hétérogène : les feuillus ont perdu leurs feuilles. Voyons s'il n'est pas possible d'affiner vos classifications en essayant d'identifier les feuillus et les résineux.

Pour cela vous n'allez classer pas toute l'image (c'est déjà fait), mais seuleument la végétation ligneuse. Il va falloir créer une image de décembre où tout ce qui n'est pas la végétation ligneuse est masqué en NoData. Cette image masquée sera ensuite de nouveau classée.


Plusieurs options sont possibles pour masquer l'image de décembre. Voici celle que je vous propose :

1. Masquez les bandes de décembre une par une à partir de la classification faite sur l'image d'octobre ;
2. Concatanez les 4 images en une seule image.

<div class="block green-block"><p class="title exercice"></p>

**Extraire les zones de forêt**

1. Masquez les bandes de décembre une par une à partir de la classification faite sur l'image d'octobre en utilisant la ==Calculatrice Raster==
2. Concatanez les 4 images en une seule image `fabas_decembre_only_forest.tif`.
3. Vérifiez que les zones masquées sont bien en NoData. Configurez la valeur de Nodata à l'aide de ==ManageNoData== si ce n'est pas le cas.

</div>

Vous allez maintenant classer `fabas_decembre_only_forest.tif`

<!-- Une possibilité pour masquer l'image de décembre aurait été d'utiliser la classification faite sur l'image d'octobre avec l'outil ==BandMath== (ou ==BandMathX== ici pour avoir une sortie avec plusieurs bandes) ou la ==Calculatrice Raster==. Cependant, les deux images de Décembre et Octobre n'ont pas la même emprise spatiale ce qui empeche l'utilisation directe de  ==BandMathX==.

Vous allez donc procéder en plusieurs étapes :
**1.** Créer un masque de forêt (à partir d'une classification de l'image de d'octobre) qui ait la même emprise spatiale que l'image de Décembre
**2.** Appliquer le masque sur l'image de décembre
**3.** Classer les feuillus et les connifères sur cette nouvelle image -->


<div class="block green-block"><p class="title exercice"></p>

**Classer les feuillus et les résineux**

1. Créez deux nouveaux fichiers d'échantillons : `train_foret.shp` et `valid_foret.shp`
2. Créez maintenant deux nouvelles classes `31` et `32` qui correspondront aux deux modes que vous avez observés plus tôt (ajoutez au moins 10 polygones dans chacune des couches train et valid) ;
3. Faites une classification de type *Knn* (sur `fabas_decembre_only_forest.tif`)
4. Appliquez un filtre majoritaire de taille 3x3 ;
5. Évaluez sa qualité
</div>

Enfin vous pouvez fusionner les deux classifications.

<div class="block green-block"><p class="title exercice"></p>

1. Fusionner les deux classifications à l'aide de la calculatrice Raster.

<div class="question">
	<label>Solution
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Si :
  - le nom de la classification d'octobre est `classif_octobre_knn_rad3.tif`
  - le label de la classe arbre est `3`
  - le nom de la classification feuillus / résineux est `classif_decembre_foret.tif`

  Alors la formule à entrer sera :
  ```
  ("classif_octobre_knn_rad3@1" != 3) * "classif_octobre_knn_rad3@1"
  +  ("classif_octobre_knn_rad3@1" = 3) * "classif_decembre_foret@1"
  ```

</span>
</label>
</div>
<br>

</div>




---------

# Ressources Annexes

- [Aide sur l'utilisation de l'OTB](s8_installation.html#utilisation-de-lotb)
- [Aide sur l'utilisation d'un terminal](aide_lcmd.html)
