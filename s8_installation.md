---
title: Télédétection - Installation
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer.less"
@import "less/blocks.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/image_zoom.less"
@import "less/link.less"
@import "less/font.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>

<center><section style="font-size:300%"> Télédétection  </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> Installation et utilisation de l'OTB </section></center>

<br>
<center><section style="font-size:150%"> Marc Lang </section></center>

<p> </p>

## OTB

[OTB](https://www.orfeo-toolbox.org/) est une bibliothèque C++ pour le traitement des images de télédétection. Elle a été développé par le [CNES](https://cnes.fr/fr/) (Agence spatiale française) pendant le programme ORFEO *préparer, accompagner et promouvoir l’utilisation et l’exploitation de la images dérivées des [Pleiades satellites (PHR)](https://en.wikipedia.org/wiki/Pl%C3%A9iades_(satellite))*. Les outils de traitement de l’OTB sont spécialement appropriés aux grandes images. Quand cela est possible les traitements sont parallélisés automatiquement. Beaucoup d’applications dérivées de l’OTB, appelées les *Applications OTB* sont directement utilisables pour les usages les plus communs. Pour les utilisateurs avancés, il est possible de développer des programmes basés sur la librairie OTB. *Monteverdi2* est *l’interface graphique* qui permet aux utilisateurs de visualiser et traiter les images satellites avec les Applications OTB. Il a été aussi développé par le CNES dans le cadre du programme ORFEO.

Dans le cadre de nos TDs, nous allons utiliser l'OTB via le système d’information géographique (SIG) QGIS que vous connaissez bien maintenant et nous verrons aussi comment utiliser les *Applications OTB* en ligne de commande.

Il faut pour cela :

1. Installer OTB sur son ordinateur ;
2. Faire le lien entre OTB et QGIS ;
3. Activer un environnement de travail dédié pour utiliser les *Applications OTB* en ligne de commande.  


## Installation de l'OTB

Tout d'abord il faut installer OTB si vous n'utilisez pas un ordinateur de l'ENSAT. Vous pouvez télécharger le logiciel [ici](https://www.orfeo-toolbox.org/download/).

### Sous Windows

L'installation consiste juste à dézipper l'archive zip que vous avez téléchargé. Je vous suggère de dé-zipper le répertoire à la racine de votre disque `C:\`.

### Sous Linux et MacOS X

Il faut décompresser l'archive `.run` :

1. ouvrir un terminal
   - (==CTRL + ALT + T==) sous Linux
   - tapez "terminal" dans le menu de recherche d'applications (Spotlight) sous MacOS X (voir [ici](https://fr.wikihow.com/ouvrir-le-Terminal-sur-un-Mac) si vous ne trouvez pas le terminal)
2. Depuis le terminal, exécutez les commandes suivantes (adapatez le nom de l'archive en fonction de fichier que vous avez téléchargé):

```bash
# 1. Se déplacer dans le dossier où a été téléchargée l'archive
cd /chemin/archive/

# 2. Rendre le fichier exécutable
chmod +x OTB-7.2.0-Linux64.run

# 3. Décompresser l'archive
./OTB-7.2.0-Linux64.run
```

<div class="block red-block">
<p class="title"> ATTENTION</p>

Le dossier d'installation est créé à l'endroit où se trouve le l'archive `.run`. Il ne faut pas déplacer le dossier résultant de l'extraction *a posteriori* sinon les applications ne fonctionneront pas. Pensez à déplacer le fichier `.run` à l'endroit où vous voulez installer l'==OTB==.
</div>




## Utilisation de l'OTB

Il y a plusieurs possibilités pour lancer les applications OTB :
1. depuis ==QGIS== et la ==boîtes à outils de traitements== ;
2. en dehors de ==QGIS==, via le lanceur d'applications Mapla ;
3. en dehors de ==QGIS==, depuis un terminal.

### Utilisation d'OTB dans QGIS

**Avantages** :
- vous pouvez lancer tous vos traitements depuis une seule fenêtre, celle de ==QGIS== ;
- les résultats de vos traitements sont directement chargés dans ==QGIS==.

**Inconvénients** :  L'OTB n'a pas été développé spécifiquement pour être utilisé dans QGIS. Il est donc possible, en fonction des versions de ==QGIS== et ==OTB==,  que l'interface entre les deux présente des bugs. C'est très souvent le cas pour l'application ==ConcatenateImages== par ex. Dans ce cas, il faudra passer par les méthodes 2 ou 3, qui permettent de lancer les applications sans passer par ==QGIS==.


#### Configurer l'interface entre QGIS et OTB

Après avoir installé OTB, il faut faire le lien entre la librairie OTB et QGIS :

- Dans ==QGIS==,  affichez la ==boîtes à outils de traitements== si elle n'est pas déjà visible : Menu ==traitements== > ==Boîte à outils de traitements==.

- cliquez sur la clé ![](img/markdown-img-paste-20200923161937860.png) ==Options== dans la ==boite à outils de traitements== ;

- puis dans ==Fournisseurs de traitements== > ==OTB==, remplir :
  - Pour **Windows** :
    - OTB application folder : `C:\chemin\repertoire\OTB-7.2.0-Win64\lib\otb\applications`
    - OTB folder : `C:\chemin\repertoire\OTB-7.2.0-Win64`

  - Pour **Linux** et **MacOS X** :
    - répertoire des applications OTB : `/chemin/repertoire/OTB-7.1.0-Linux64/lib/otb/applications`
    - répertoire OTB : `/chemin/repertoire/OTB-7.1.0-Linux64`

<div class="block blue-block">
<p class="title"> NOTE</p>

Notez bien que dans les chemins cités en exemple, il faut que vous remplassiez  `C:\chemin\repertoire\` ou `/chemin/repertoire/` par le chemin du dossier d'installation d'OTB de **votre** machine, par exemple sur ma machine Linux c'est `/home/terudel/otb/OTB-7.0.0-Linux64`. Pour un Windows 64 bit, si vous avez dézippé l'archive zip à la racine ce sera : `C:\OTB-7.2.0-Win64`

</div>

<div class="block blue-block">
<p class="title"> Astuce</p>

Lorsque vous saisissez les chemins d'accès à OTB **(1)**, cliquez à l'extérieur du champs de saisie une fois la saisie effectuée **(2)** et non directement sur ==OK==. Cliquez enfin sur ==OK== **(3)** pour que la saisie soit bien pris en compte. Pour finir penser à **bien cocher la case Activer** si ce n'est pas déjà fait.

![](img/install_lien_qgis.png)

</div>

#### Lancer les applications OTB dans QGIS

Une fois le lien entre ==QGIS== et ==OTB== réalisé, vous pouvez vérifier ensuite que vous avez bien accès aux traitements de l'==OTB== dans la ==boîte à outils de traitements==:

<center><img src="img/markdown-img-paste-20210211163836208.png" title="" width="40%"></center>


### Utililisation de l'OTB avec le lanceur d'applications Mapla

**Avantages** : Vous pouvez lancer tous vos traitements sans configurer l'interface entre ==QGIS== et ==OTB==. Pratique.

**Inconvénients** : Pour visualiser les résultats de vos traitements, vous devrez ouvrir à chaque fois les images produites dans QGIS.

#### Sous Linux

1. ouvrir un terminal (==CTRL + ALT + T==) ;
2. se déplacer dans le dossier d'installation  `cd /chemin/repertoire/` ;
3. executer le fichier `mapla.sh` : `source mapla.sh` ;
4. L'interface graphique devrait s'ouvir.

Exemple :

```bash
# déplacement (le chemin d'accès est un exemple)
cd /usr/local/OTB-7.1.0-Linux64

# Voir les fichiers présents
ls

# Exécuter le fichier de configuration
source mapla.sh
```

#### Sous Windows

1. Allez dans le dossier d'installation de l'OTB
2. Repérez le fichier `mapla.bat`
3. ==Clique droit== > ==exécuter== sur le fichier `mapla.bat`
4. L'interface graphique devrait s'ouvir.

#### Sous MacOS X

1. Allez dans le dossier d'installation de l'OTB
2. Repérez le fichier `Mapla.app`
3. ==Double Clique== sur le fichier `Mapla.app`
4. L'interface graphique devrait s'ouvir.


### Utilisation de l'OTB (et GDAL) en ligne de commande

**Avantages** :
- Vous pouvez lancer tous vos traitements sans configurer l'interface entre ==QGIS== et ==OTB== ;
- vous pouvez garder l'historique de vos traitements dans un fichier texte et revenir dessus plus tard ;
- vous pouvez ré-utiliser une chaîne de traitements en faisant un simple copier-coller depuis un ficher texte contenant vos traitements passés vers un terminal.

**Inconvénients** :
- Il faut se familiariser avec l'usage d'un terminal ;
- Il faudra d'abord configurer votre terminal pour qu'il puisse retrouver les applications OTB.

#### Configuration du terminal

Il faudra faire cette opération à chaque fois que vous ouvrez un terminal.

##### Sous Linux

1. ouvrir un terminal (==CTRL + ALT + T==)
2. se déplacer dans le dossier d'installation  `cd /chemin/repertoire/`
3. exécuter le fichier `otbenv.profile` : `source otbenv.profile`

```bash
# déplacement (le chemin d'accès est un exemple)
cd /usr/local/OTB-7.1.0-Linux64

# Voir les fichiers présents
ls

# Exécuter le fichier de configuration
source otbenv.profile
```

Vous pouvez maintenant accéder aux applications `otbcli`, `otbgui` et aux applications ==GDAL==

##### Sous windows


1. Ouvrir un terminal : ==Menu démarrer== > ==Anaconda Prompt== (attention ce n'est pas le prompt PowerShell qu'il faut lancer)
2. se déplacer dans le dossier d'installation  `cd \chemin\repertoire\`
3. executer le fichier `otbenv.bat` : `otbenv.bat`

```bash
# déplacement (le chemin d'accès est un exemple)
cd `C:\OTB-7.2.0-Win64`

# Voir les fichiers présents
dir

# Exécuter le fichier de configuration
otbenv.bat
```

Vous pouvez maintenant accéder aux applications `otbcli`, `otbgui` et aux applications ==GDAL==

#### Lancer les applications depuis un terminal

Une fois votre terminal configuré, vous pouvez lancer les applications en ligne de commande (`otbcli_NomDeLapplication`).

**Exemple** : Obtenir des informations sur une image avec l'application ==ReadImageInfo==.

Si vous entrez le nom de l'application, sans paramètres :
```bash
otbcli_ReadImageInfo
```

vous obtiendrez un message d'erreur avec une aide sur l'utilisation de l'application :
```bash
ERROR: Waiting for at least one parameter.


This is the ReadImageInfo application, version 6.7.0

Get information about the image
Complete documentation: https://www.orfeo-toolbox.org/CookBook/Applications/app_ReadImageInfo.html or -help

Parameters:
MISSING -in          <string>         Input Image  (mandatory)
        -keywordlist <boolean>        Display the OSSIM keywordlist  (mandatory, default value is false)
        -outkwl      <string>         Write the OSSIM keywordlist to a geom file  (optional, off by default)
        -rgb         <group>          Default RGB Display
        -progress    <boolean>        Report progress
        -help        <string list>    Display long help (empty list), or help for given parameters keys

Use -help param1 [... paramN] to see detailed documentation of those parameters.

Examples:
otbcli_ReadImageInfo -in QB_Toulouse_Ortho_XS.tif
```

Il fallait ici renseigner le chemin de l'image pour laquelle on souhaite obtenir des informations à l'aide du paramètre `-in` :

```bash
otbcli_ReadImageInfo -in /home/terudel/Documents/cours/2020-2021/ENSAT/2A_S8/data/Pleiades/fabas_10_12_2013.tif
```
et on obtiendra en sortie quelque chose dans ce gout là :

```bash
2021-04-01 10:29:53 (INFO) ReadImageInfo: Default RAM limit for OTB is 256 MB
2021-04-01 10:29:53 (INFO) ReadImageInfo: GDAL maximum cache size is 1596 MB
2021-04-01 10:29:53 (INFO) ReadImageInfo: OTB will use at most 12 threads
2021-04-01 10:29:53 (INFO) ReadImageInfo:
Image general information:
	Number of bands : 4
	Data type : unsigned_short
	No data flags : Not found
	Start index :  [0,0]
	Size :  [3390,2074]
	Origin :  [522844,6.24866e+06]
	Spacing :  [2,-2]
	Estimated ground spacing (in meters): [1.99341,2.00065]

Image acquisition information:
	Sensor :
	Image identification number:
	Image projection : PROJCS["2154 RGF93 / Lambert93 (FR.)",
    GEOGCS["RGF93 (FR.) [4171]",
        DATUM["unknown",
            SPHEROID["GRS 1980",6378137,298.2572221010042,
                AUTHORITY["EPSG","7019"]]],
        PRIMEM["Greenwich",0],
        UNIT[,0.0174532925199433]],
    PROJECTION["Lambert_Conformal_Conic_2SP"],
    PARAMETER["standard_parallel_1",44],
    PARAMETER["standard_parallel_2",49],
    PARAMETER["latitude_of_origin",46.5],
    PARAMETER["central_meridian",3],
    PARAMETER["false_easting",700000],
    PARAMETER["false_northing",6600000],
    UNIT["metre",1,
        AUTHORITY["EPSG","9001"]]]

Image default RGB composition:
	[R, G, B] = [0,1,2]

Ground control points information:
	Number of GCPs = 0
	GCPs projection =

Output parameters value:
indexx: 0
indexy: 0
sizex: 3390
sizey: 2074
spacingx: 2
spacingy: -2
originx: 522844
originy: 6248662
estimatedgroundspacingx: 1.993409157
estimatedgroundspacingy: 2.000645876
numberbands: 4
datatype: unsigned_short
sensor:
id:
time:
town:
country:
rgb.r: 0
rgb.g: 1
rgb.b: 2
projectionref: PROJCS["2154 RGF93 / Lambert93 (FR.)",
    GEOGCS["RGF93 (FR.) [4171]",
        DATUM["unknown",
            SPHEROID["GRS 1980",6378137,298.2572221010042,
                AUTHORITY["EPSG","7019"]]],
        PRIMEM["Greenwich",0],
        UNIT[,0.0174532925199433]],
    PROJECTION["Lambert_Conformal_Conic_2SP"],
    PARAMETER["standard_parallel_1",44],
    PARAMETER["standard_parallel_2",49],
    PARAMETER["latitude_of_origin",46.5],
    PARAMETER["central_meridian",3],
    PARAMETER["false_easting",700000],
    PARAMETER["false_northing",6600000],
    UNIT["metre",1,
        AUTHORITY["EPSG","9001"]]]
keyword:
gcp.count: 0
gcp.proj:
gcp.ids:
gcp.info:
gcp.imcoord:
gcp.geocoord:
```
