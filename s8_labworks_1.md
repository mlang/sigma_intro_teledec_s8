---
title: Télédétection - TD1
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer2.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/image_zoom.less"
@import "less/iframe.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/blocks.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"

<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection  </section></center>
<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD 1 : Visualisation des images </section></center>
<br>
<center><section style="font-size:150%"> Marc Lang </section></center>
<p> </p>

<img src="img/markdown-img-paste-20210213132807297.png" title="" width="125%">

<div class="question">
	<label> Curiosité n°1 : D'après vous, de quoi s'agit-il ?
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Cette image est issue de la [gallerie d'images d'Airbus](https://www.intelligence-airbusds.com/satellite-image-gallery/). C'est une image issue du satellite Pléiades (50cm de résolution spatiale) acquise autour de la ville de Toulouse.

</span>
</label>
<br><br>
</div>

<section style="font-size:200%"> Contenu </section></center>

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=4 orderedList=false} -->
<!-- code_chunk_output -->

  - [Téléchargement des données](#téléchargement-des-données)
  - [Découverte des images de télédétection](#découverte-des-images-de-télédétection)
  - [Obtenir des informations sur les images](#obtenir-des-informations-sur-les-images)
  - [Visualisation](#visualisation)
    - [Visualisation d'une bande en niveau de gris](#visualisation-dune-bande-en-niveau-de-gris)
    - [Composition colorée](#composition-colorée)
    - [Améliorer le contraste de l'image](#améliorer-le-contraste-de-limage)
  - [Relever de l'information spectrale](#relever-de-linformation-spectrale)
- [Ressources Annexes](#ressources-annexes)

<!-- /code_chunk_output -->

## Téléchargement des données

Vous pouvez télécharger les données nécessaires à ce TD [ici](https://filesender.renater.fr/?s=download&token=c2015821-0067-43f4-b90f-f8e9ab851c32).

## Découverte des images de télédétection

Dans cette partie vous allez découvrir vos premières images de télédétection. L'objectif est de repérer de manière "naïve" les différences entre les images qui vous sont proposées. Pour vous aider, voulez utiliser l'outil ![](img/markdown-img-paste-20200923171803945.png)  ==Identifier les entités== que vous connaissez déjà. Il vous sert à inspecter les attributs d'entités vecteurs, il sert également à inspecter les valeurs d'un pixel.

<div class="block green-block"><p class="title exercice"></p>

1. Ouvrez le projet Qgis `decouverte.qgz`.
2. Utilisez l'outil ==Identifier les entités== sur une des six images de votre choix : combien de valeurs contient un pixel ?
3. Notez les différences entre les images que vous observez avec vos propres mots (taille, forme, impression, etc. )
4. De manière plus précise,  quelle(s) différence(s) observez vous entre :
  4.1. les images A et B ;
  4.2. les images 3 et 2 ;
  4.3. les images 2 et 1 ;
  4.4. les images 4 et 5 ;
  4.5. les images 4 et 6.

</div>

## Obtenir des informations sur les images


Avant même d'ouvrir des images de télédétection, il est possible d'obtenir des informations sur leurs propriétés à l'aide de l'application ==gdalinfo== :

- en ligne de commande :

  ```bash
  gdalinfo fabas_10_12_2013.tif
  ```
- depuis QGIS : ==Boîte à outils de traitements== > ==GDAL== > ==Divers raster== > ==Information raster==

Il est également possible d'obtenir les mêmes informations en utilisant l'application OTB [==ReadImageInfo==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ReadImageInfo.html) :

- dans une ligne de commande :
  ```
  otbcli_ReadImageInfo -in fabas_10_12_2013.tif
  ```
- depuis QGIS : ==Boîte à outils de traitements== > ==OTB== > ==Image Manipulation== > ==ReadImageInfo==



<div class="block blue-block">
<p class="title"> RAPPEL</p>

Pour lancer une application otb en ligne de commande il faut tout d'abord [activer l'OTB](s8_installation.html#sous-windows)

</div>

Enfin, ces informations sont disponibles dans QGIS (via GDAL). Pour les obtenir il faut :
- Ouvrir `fabas_10_12_2013.tif` dans QGIS
- ==Clique droit== sur une couche > ==propriétés== > ==Information==

<div class="block green-block"><p class="title exercice"></p>

Sur l'image de *Fabas*, cherchez les informations suivantes avec les trois méthodes proposées :

1. Nombre de lignes, colonnes et bandes ;
2. Taille des pixels ;
3. Encodage de l'image ;
4. Origine de l'image ;
5. Projection.

</div>


## Visualisation

Vous allez maintenant visualiser dans QGIS des images de télédétection. Les images que vous allez utiliser sont des images du satellite Pléiades, acquises le 12 octobre 2013 et le 10 décembre 2013. Les images sont fournies au format [GeoTIFF](https://trac.osgeo.org/geotiff/).

### Visualisation d'une bande en niveau de gris

La première étape que vous allez faire pour prendre en main les images que vous avez à disposition est de visualiser les bandes une par une en niveau de gris. L'objectif est de déterminer à quel domaine spectrale correspond chaque bande de l'image. Vous pouvez pour cela pour procéder de plusieurs manières :

1. En regardant l'aspect général de l'image pour une bande donnée :
   - en l'afffichant en niveau de gris :   ==Clique droit== sur l'image > ==Propriétés== > ==Symbologie== > ==Rendu des bandes raster== > ==Bande Grise Unique==
   - en améliorant éventuellement le contraste

<figure>
	<img src="img/amelioration_contraste.png" title="" width="80%">
	<figcaption>Amélioration du contraste</figcaption>
</figure>


2. En utilisant l'outil ==ValueTool== (un plugin à installer) qui est un outil similaire à ==Indentifier les identités== mais qui permet de relever l'information spectrales de pixels de manière plus aisée et sous forme de graphique :


<figure>
	<img src="img/value_tool_spectra.png" title="" width="50%">
	<figcaption>Utilisation du mode Graph de Value Tool</figcaption>
</figure>



<div class="block green-block"><p class="title exercice"></p>

0. Ouvrez un nouveau projet Qgis ;
1. Ouvrez l'image `fabas_12_10_2013.tif` dans Qgis ;
2. Affichez une bande en niveau de gris ;
3. Améliorez son contraste :

<figure>
	<center><img src="img/fabas_gris.png" title="" width="75%">
	<figcaption>Exemple d'affichage contrasté de la bande Rouge</figcaption></center>
</figure>


4. Regardez quelques valeurs de pixels à l'aide de l'outil ==ValueTool==
5. Avec vos nouvelles connaissances et les outils proposées, trouvez à quel domaine spectral correspond chacune des bandes de l'image de manière à remplir le tableau suivant :

<div class="my_table">

| Numéro de bande | Domaine spectrale (bleu, rouge, etc ?) |
|:---------------:|:--------------------------------------:|
|        1        |                   ?                    |
|        2        |                   ?                    |
|        3        |                   ?                    |
|        4        |                   ?                    |

<center><div class="table-caption">Correspondance entre Numéro de bande et domaine spectrale.</div></center>
</div>
<br>
<div class="question">
	<label>Indice pour trouver la bande "Infra-rouge"
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">
  La végétation à une reflectance maximale dans l'infra-rouge
	</span>
	</label>
</div>
<br>
<div class="question">
	<label>Indice pour trouver la bande "Rouge"
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">
  La végétation absorbe beaucoup le rouge.
	</span>
	</label>
</div>
<br>
<div class="question">
	<label>Indice pour trouver la bande "Bleu"
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">
  L'eau à une reflecance maximal dans le Bleu.
	</span>
	</label>
</div>
<br>
</div>

### Composition colorée

Maintenant que vous êtes plus à l'aise avec les images du TD, vous allez affichez plusieurs compositions colorées à l'aide de l'affichage ==Couleur à bandes multiples== : ==Clique droit== sur l'image > ==Propriétés== > ==Symbologie== > ==Rendu des bandes raster== > ==Couleur à bandes multiples==. Il s'agit ensuite d'attribuer à chaque canal (Bande rouge, Bande verte ou Bande bleue) les bandes de l'image de votre choix.

Pour rappel une composition en vraies couleurs correspond à l'association entre les canaux et les domaines spectraux suivants :


<div class="my_table">

| Canal | Domaine spectrale |
|:-----:|:-----------------:|
| Rouge |       Rouge       |
| Vert  |       Vert        |
| Bleu  |       Bleu        |

<center><div class="table-caption"> Composition colorée vraies couleurs.</div></center></div>

Pour obtenir un affichage "naturel" :

<figure>
	<center><img src="img/markdown-img-paste-20210212095850472.png" title="" width="90%">
	<figcaption>Affichage en composition colorée vraies couleurs.</figcaption></center>
</figure>


Pour rappel une composition en IRC fausses couleurs correspond à l'association entre les canaux et les domaines spectraux suivants :


<div class="my_table">

| Canal | Domaine spectrale |
|:-----:|:-----------------:|
| Rouge |    Infra-Rouge    |
| Vert  |       Rouge       |
| Bleu  |       Vert        |

<center><div class="table-caption">Composition colorée IRC fausse couleurs.</div></center></div>

Pour obtenir un affichage où la végétation est mise en avant (en rouge) :

<figure>
<center><img src="img/markdown-img-paste-20210212100736387.png" title="" width="90%">
<figcaption>Affichage en composition colorée IRC fausses couleurs.</figcaption></center>
</figure>

<div class="block green-block"><p class="title exercice"></p>

Dans l'exercice précédent vous avez identifié les domaines spectraux des bandes des images Pléiades. Vous pouvez donc faire des compositions colorées en connaissance de cause.  

Sur l'image `fabas_12_10_2013.tif` :
1. Effectuez une composition colorée **vraies couleurs** (RVB) ;
2. Effectuer une composition colorée **fausses couleurs** (IRC) ;
3. Ouvrir maintenant l'image `fabas_10_12_2013.tif` et effectuer une composition colorée **fausses couleurs** (IRC) ;
4. Comparer les deux images à l'aide de l'outil [==MapSwipeTool==](https://plugins.qgis.org/plugins/mapswipetool_plugin/) <img src="img/markdown-img-paste-20200923100919462.png" title="" width="25px"> (Plugin à installer) : Quelle**s** différence**s** observez vous ?

<figure>
	<img src="img/map_swipe_tool3.gif" title="" width="100%">
	<figcaption>Exemple d'utilisation de Map Swipe Tool.</figcaption>
</figure>


</div>


### Améliorer le contraste de l'image

Dans la partie 4.1 [Visualisation d'une bande en niveau de gris](#visualisation-dune-bande-en-niveau-de-gris), nous avons vu de manière très rapide comment améliorer le contraste d'une image. Revenons un peu plus en détails sur cette notion.

Pour rappel, une image est caractérisée (entre autres) par sa résolution radiométrique et donc une plage de valeurs possibles (voir tableau ci-dessous).


<div class="my_table">

| Type(en python) |                          Plage de valeurs                           | Taille d’une image 1000 x1000 |
|:---------------:|:-------------------------------------------------------------------:|:-----------------------------:|
|  uInt8 - int8   |                        0 à 255 ou -127 à 127                        |            ~ 1 Mo             |
| uInt16 – int16  |                    0 à 65535 ou - 32778 à 32778                     |            ~ 2 Mo             |
| uInt32 – int32  |             0 à 4294967295 ou -2147483647 à 2147483647              |            ~ 4 Mo             |
|     float32     |     1.5 x 10$^{-45}$ à 3.4 x 10$^{38}$ précision de 7 chiffres      |            ~ 4 Mo             |
|     float64     | 1.5 x 10$^{-345}$ à 3.4 x 10$^{308}$ précision de 15 ou 16 chiffres |            ~ 8 Mo             |

<center><div class="table-caption">Encodages des images.</div></center></div>

Les images Pléiades que vous utilisez sont codées en **UInt16 - nombre entier non signé de seize bits**, ce qui veut dire que les valeurs de l'image sont comprises entre 0 à 65535. On pourrait donc imaginer configurer l'affichage d'une bande dans QGIS en réglant le minimum des valeurs à afficher à 0 et le maximum à 65535 (voir Figure ci-dessous).

<figure>
	<center><img class="zoomable" src="img/hist_basic.png" title="" width="100%"></center>
	<figcaption>Aucune amélioration du contraste.</figcaption>
</figure>


Or en pratique, la valeur maximale d'une bande correspond rarement à la valeur maximale autorisée par l'encodage. On peut voir par exemple sur l'histogramme de la bande IR ci-dessus que la valeur maximale de la bande IR est largement inférieure à 65535 : l'histogramme de la bande est donc complétement aplati et le rendu visuel est noir. **Il faut donc configurer l'affichage de chaque bande en fonction de sa dynamique !**

Regardons ce qu'il se passe si on configure manuellement cette fois ci les valeurs maximales avec une valeur plus faible, qu'on imagine plus proche de la dynamique des bandes. Prenons par exemple une valeur maximale de 255 :

<figure>
	<center><img class="zoomable" src="img/histo_manuelle.png" title="" width="100%"></center>
	<figcaption>Configuration manuelle du contraste.</figcaption>
</figure>


Il est cette fois-ci possible de visualiser l'image mais notre configuration manuelle n'est pas tout à fait satisfaisante. La plage de valeurs utilisée par QGIS (0-255) est beaucoup plus restreinte que la plage de valeur de la bande IR (à peu près 0 - 1750 d'après l'histogramme de la figure ci-dessus): l'image apparait trop lumineuse.

On pourrait finir par trouver une configuration satisfaisante par tatonnement mais QGIS nous permet directement d'adapter l'affichage à la dynamique des bandes grâce à l'option ==Paramètres de valeurs Min/Max== > ==Min/Max== :

<figure>
	<img class="zoomable" src="img/histo_minmax.png" title="" width="100%">
	<figcaption>Amélioration du contraste selon le minimum et maximum de chaque bande. </figcaption>
</figure>


On obtient cette fois un affichage correspondant à la dynamique de l'image, automatiquement.

Il est toutefois possible d'améliorer le contraste de l'image en réduisant la plage de valeurs utilisée par QGIS. Il est d'usage d'utiliser comme minimum et maximum non plus les minimum et maximum mais le 2 ième et le 98ième quantile grâce à l'option ==Paramètres de valeurs Min/Max== > ==Cumulatif décompte de coupe==:

<figure>
	<img class="zoomable" src="img/histo_percentile.png" title="" width="100%">
	<figcaption>Amélioration du contraste selon le 2ième et 98ième quantile de chaque bande. </figcaption>
</figure>


Pour plus de détails sur l'amélioration du contraste vous pouvez consulter la [Documatation de QQIS](https://docs.qgis.org/3.10/fr/docs/user_manual/working_with_raster/raster_properties.html#symbology-properties) à ce propos.

<div class="block green-block"><p class="title exercice"></p>

Testez, sur une zone de votre choix, les différentes configurations d'affichage proposées ci-dessous :

1. Configuration manuelle :
   1. Plage de valeurs de 0 à 65535 :
   2. Plage de valeurs de 0 à 255 ;
2. Configuration automatique :
   1. Selon Min / Max ;
   2. Selon le 2 ième et le 98 ième quantile.

</div>



## Relever de l'information spectrale

Maintenant que vous savez modifier la configuration de l'affichage d'images de télédétection, vous allez regarder le comportement spectrale des différentes occupations du sol présentes dans l'image. Nous afficherons ensuite l'histogramme des valeurs des pixels que vous aurez relevez.

<div class="block green-block"><p class="title exercice"></p>

Sur l'image `fabas_10_12_2013.tif`, relevez l'information spectrale à l'aide de l'outil ==Value Tool== (Plugin à installer) des objets suivants (3 à 5 pixels par occupation du sol) :
- eau ;
- forêt ;
- sol nu ;
- culture.

Vous remplirez les valeurs que vous relevez dans la feuille de calcul ci-dessous.
</div>

<iframe src="https://lite.framacalc.org/9lhm-s8_teledetection" height="700" frameborder=0 ></iframe>


-----------
# Ressources Annexes

- [Aide sur l'utilisation de l'OTB](s8_installation.html#utilisation-de-lotb)
- [Aide sur l'utilisation d'un terminal](aide_lcmd.html)
