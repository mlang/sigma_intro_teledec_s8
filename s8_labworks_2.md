---
title: Télédétection - TD2
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer2.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/image_zoom.less"
@import "less/iframe.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/blocks.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection  </section></center>
<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD 2 :  Pré-traitements des images satellites </section></center>
<br>
<center><section style="font-size:150%"> Marc Lang </section></center>
<p> </p>

<img src="img/r59727_39_worlddem-satellite-image-la-reunion-overseas-department-of-france-05112019-4.jpg" title="" width="125%">
<div class="question">
	<label> Curiosité n°2 : D'après vous, de quoi s'agit-il ?
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Cette image est encore issue de la [gallerie d'images d'Airbus](https://www.intelligence-airbusds.com/satellite-image-gallery/). C'est une image issue du satellite Pléiades (50cm de résolution spatiale) acquise autour de la ville de Toulouse.

</span>
</label>
<br><br>
</div>


<section style="font-size:200%"> Contenu </section></center>


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=4 orderedList=False} -->
<!-- code_chunk_output -->

  - [Les données](#les-données)
  - [Principe](#principe)
  - [Que sont les valeurs NoData ?](#que-sont-les-valeurs-nodata)
  - [Gestion du NoData de manière simple et non-définitive](#gestion-du-nodata-de-manière-simple-et-non-définitive)
  - [Changement de l'ordre des bandes](#changement-de-lordre-des-bandes)
  - [Gestion du NoData de manière définitive](#gestion-du-nodata-de-manière-définitive)
  - [Emprise et masque de NoData](#emprise-et-masque-de-nodata)
- [Ressources Annexes](#ressources-annexes)

<!-- /code_chunk_output -->

## Les données

Vous pouvez télécharger les données du TD --> [ici](https://filesender.renater.fr/?s=download&token=78f1ca97-d0a8-435a-a230-02b3d4fe7f0e) <--.

<div class="block green-block"><p class="title exercice"></p>

1. Téléchargez les données ;
2. Ouvrez le précédent projet QGIS.

</div>

## Principe

Les images multi-spectrales ne sont pas toujours livrées au format, dans le sens général du terme, souhaité pour l'application en cours. Une image peut être :
- livrée en plusieurs bandes (une image par bande) ;
- avec plus de bandes que nécessaire ;
- selon une emprise beaucoup plus grande que nécessaire ;
- etc.

Dans ce TD, nous allons voir quelques opérations de manipulation courantes d'images pour préparer une image à des traitements futurs. Ces opérations sont :
- la **séparation** d'une image multi-bandes en plusieurs images mono-bande ;
- la **concaténation**, qui consiste à fusionner l'ensemble des images mono-bande en une seule image multi-bandes (ou multi-spectrale), elle implique que l'ensemble des bandes aient une même résolution spatiale ;
- le **découpage**, qui consite à découper l'image selon une zone d'étude,
- la **gestion des NoData**, qui permet de renseigner dans les meta-données les pixels qui ne contiennent pas d'information.


## Que sont les valeurs NoData ?

Il est possible que certaines zones d'une image ne contiennent pas d'information et ce pour plusieurs raisons :

- à cause de l'emprise de l'acquisition ;

<figure>
	<img src="img/acquisition.png" title="" width="75%">
	<figcaption>Toute l'image ne correspond pas forcément à de la donnée.</figcaption>
</figure>


- un masque à été appliqué sur l'image.

Cependant, chaque pixel d'une image contient une valeur, il est alors d'usage d'attribuer la même valeur à ces pixels non informatifs. On leur atribue généralement la valeur maximale possible (pax ex. 65535 pour une image en entier 16 bit non signée) ou une valeur particulière (par ex. 0 ou -9999).

Ces valeurs particulière ne doivent pas être prises en compte pour les différents traitements réalisés sur les images puisqu'elle ne correspondent pas à des valeurs de réflectance (ou luminance). Pour cela, il est possible de renseigner dans les **métadonnées de l'image** quelle est cette valeur de NoData.

Vous pouvez consultez la valeur de NoData d'une image à l'aide de l'application ==ReadImageInfos== de l'OTB.

<div class="block green-block"><p class="title exercice"></p>

  - Ouvrez l'image `fabas_12_10_2013_V2.tif` ;
  - Regardez la valeur de NoData à l'aide de l'application ==ReadImageInfos== ;
  - Cette information est visible dans l'onglet ==Journal==, après avoir exécuté ==ReadImageInfos== sur `fabas_12_10_2013_V2.tif` :

  <figure>
	<img src="img/markdown-img-paste-20210401120954404.png" title="" width="100%">
	<figcaption>Reperer la valeur de NoData inscrite dans les métadonnées</figcaption>
  </figure>

  - Y-a-t-il une valeur de NoData renseignée dans les métadonnées de l'image `fabas_12_10_2013_V2.tif` ?

  <div class="question">
	<label>Réponse
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Non

</span>
</label>
</div>

<br>
</div>

## Gestion du NoData de manière simple et non-définitive

Vous avez vu dans la partie précédente qu'il n'y a pas de valeur de NoData renseignée dans les métadonnées de l'image `fabas_12_10_2013_V2.tif`. Pour autant, cette image contient bien des zones sans données de réflectance, saurez-vous les repérer ?

<div class="block green-block"><p class="title exercice"></p>

Constatez dans un premier temps qu'il existe des zones sans donnée dans les images du TD :
   - Ouvrez l'image `fabas_12_10_2013_V2.tif` ;
   - Que pouvez vous dire de l'affichage de l'image ?
   - Comparez les valeurs cette image avec `fabas_12_10_2013.tif`: les valeurs sont elles les même ?
   - Alors à votre avis pourquoi l'affichage n'est pas correcte ? Regardez les valeurs configurées dans le **style de la couche** pour vous faire une idée.
   - Essayez de changer **manuellement** pour obtenir un affichage qui vous convient.
   - Mieux : vous avez configuré l'affichage de `fabas_12_10_2013.tif` lors du [précédent TD](s8_labworks_1.html#améliorer-le-contraste-de-limage), vous pouvez automatiquement appliquer le style de l'image `fabas_12_10_2013.tif` à l'image `fabas_12_10_2013_V2.tif` :
     1. Dans le panneaux des couches
     2. ==Clique droit== sur `fabas_12_10_2013.tif` > ==Styles== > ==Copier le style==
     3. ==Clique droit== sur `fabas_12_10_2013_V2.tif` > ==Styles== > ==Coller le style==

</div>

Vous avez peut être remarqué que **l'amélioration automatique de contraste** ([Cf précédent TD](s8_labworks_1.html#am%C3%A9liorer-le-contraste-de-limage.html)) ne fonctionnait pas avec la couche  `fabas_12_10_2013_V2.tif`.

Par défaut, QGIS ne sait pas si l'image que vous affichez contient des zones sans données. Il faut lui renseigner une valeur de **NoData**, c'est à dire lui indiquer quelle valeur il ne faut pas afficher, car elle n'est pas une vraie valeur. Pour ce faire il est possible de le faire de manière non permanente.

<div class="block green-block"><p class="title exercice"></p>

1. Renseigner une valeur de **NoData** :
   - trouver quelle est la valeur de **NoData** (avec ==Value Tool==).
   - la renseigner (==Clique droit sur la couche== > ==Propriétés== > ==Transparence== > ==Aucune Valeur de données==)
2. Régler le contraste de l'image.

</div>

Vous avez ici configuré QGIS pour ne pas prendre en compte les zones sans donnée dans l'affichage de l'image. Il faut bien comprendre que **cette opération est temporaire** et ne concerne que l'affichage de l'image.

<div class="block green-block"><p class="title exercice"></p>

Pour s'en convaincre :

1. Regardez la valeur de NoData à l'aide de l'application ==ReadImageInfos== ;
2. Y-a-t-il une valeur de NoData renseignée dans les métadonnées de l'image `fabas_12_10_2013_V2.tif` ?

<div class="question">
<label>Réponse
<input type="checkbox">
<i class="arrow"></i>
<span class="reponse">

Toujours pas

</span>
</label>
</div>
<br>

   3. Supprimez l'image `fabas_12_10_2013_V2.tif` de QGIS puis rechargez là.
   4. Que constatez vous ?

 <div class="question">
 <label>Réponse
 <input type="checkbox">
 <i class="arrow"></i>
 <span class="reponse">

 Vous avez de nouveau un problème d'affichage que vous devez encore régler  (==Clique droit sur la couche== > ==Propriétés== > ==Transparence== > ==Aucune Valeur de données==)

 </span>
 </label>
 </div>
 <br>

</div>

On se contentera de cette solution pour le moment.


## Changement de l'ordre des bandes

Vous avez pu constater dans le précédent TD que l'ordre croissant des bandes des images _Pléiades_ ne correspondait pas à l'ordre croissant des domaines spectraux. L'objectif de cette partie est de construire un raster avec un ordre de bandes différents :

<div class="my_table">

| Numéro de bande | Domaine spectrale initiale | Domaine spectrale après traitement |
|:---------------:|:--------------------------:|:----------------------------------:|
|        1        |           Rouge            |                Bleu                |
|        2        |            Vert            |                Vert                |
|        3        |            Bleu            |               Rouge                |
|        4        |        Infra-rouge         |            Infra-rouge             |

<center><div class="table-caption">Ordre des bandes de l'image Fabas avant et après traitement.</div></center></div>


Vous allez dans un premier temps **séparer** l'image multi-spectrale en plusieurs images mono-bande puis les **ré-assembler** en une image avec l'ordre convenu (voir Tableau ci-dessus).


<figure>
	<img src="img/chgt_ordre_bande.png" title="" width="90%">
	<figcaption>Étapes pour le changement de l'ordre des bandes</figcaption>
</figure>


<div class="block green-block"><p class="title exercice"></p>

**1.a.** **Séparez** l'image `fabas_12_10_2013_V2.tif` en plusieurs images à l'aide de l'application OTB [==SplitImage==](https://www.orfeo-toolbox.org/CookBook/Applications/app_SplitImage.html) :
   - soit en ligne de commande `otbcli_SplitImage` ;
   - soit dans la boîte à outils :  ==ToolBox== > ==OTB== > ==Image Manipulation== >[==SplitImage==](https://www.orfeo-toolbox.org/CookBook/Applications/app_SplitImage.html) ;
   - **Remarque** : attention au **type** de l'image en sortie (Byte, Uint16, float, etc ...) ;
   - Exemple en ligne de commande :

   ```bash
   otbcli_SplitImage -in fabas_12_10_2013_V2.tif -out ../results/fabas_12_10_2013_V2_split.tif uint16
   ```
**1.b.** **Vérifiez** que le traitement a bien fonctionné :
  - ouvrez les quatre images monobande dans QGIS ;
  - à l'aide de ==Value Tool==, vérifier à quelle bande correspond chaque image monobande.

**2.a** **Concaténez** l'ensemble des bandes spectrales en une seule image grâce à l'application [==ConcatenateImages==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ConcatenateImages.html) (nommez l'image en sortie `fabas_12_10_2013_V2_reordered.tif`):
   - soit en ligne de commande `otbcli_ConcatenateImages` ;
   - soit dans la boîte à outils :  ==ToolBox== > ==OTB== > ==Image Manipulation== >[==ConcatenateImages==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ConcatenateImages.html) ;
   - Exemple en ligne de commande :

   ```bash
   otbcli_ConcatenateImages -il ../results/fabas_12_10_2013_V2_split_2.tif ../results/fabas_12_10_2013_V2_split_1.tif ../results/fabas_12_10_2013_V2_split_0.tif ../results/fabas_12_10_2013_V2_split_3.tif -out fabas_12_10_2013_V2_reordered.tif uint16
   ```
  <div class="block blue-block"><p class="title" style="background: #6ab0de"> Astuce</p>

  Dépendamment des versions de QGIS et OTB, l'application ==ConcatenateImages== utilisée dans QGIS peut être capricieuse. Si l'opération n'a pas fonctionné comme escompté, répétez l'opération en faisant bien atttention à :
  - l'ordre dans lequel vous sélectionnez les bandes à concaténer dans la *fenêtre de traitement* ;
  - l'ordre des bandes dans le *panneau de couches*.

  </div>

**2.b.** **Vérifiez** que le traitement a bien fonctionné :
   - ouvrez la nouvelle image multi-bande dans QGIS ;
   - à l'aide de ==Value Tool==, vérifiez que la bande 1 de la nouvelle image correspond à la bande 3 de l'ancienne image, etc.

**3.** Faire une composition colorée de votre nouvelle image :
   - Vraies couleurs **(R : R, V: V, B: B)** ;
   - Fausses couleurs **(R : IR, V : R, B: V)**.

</div>

## Gestion du NoData de manière définitive

Il est posssible de renseigner dans les méta-données la valeur de **NoData** de l'image. QGIS reconnaîtra ainsi directement ces valeurs et ne les considerera pas ni pour les traitements raster futurs, ni pour la configuration de l'affichage.

Vous allez pour cela utiliser l'application ==ManageNoData== d'OTB. Cette application est utilisable selon trois modes différents.


<div class="block green-block"><p class="title exercice"></p>

La doc est disponible [ici](https://www.orfeo-toolbox.org/CookBook/Applications/app_ManageNoData.html?highlight=data%20type#change-the-no-data-value-options). Gardez en tête l'objectif de cette partie et essayez de déterminer le mode à utiliser en lisant la documentation.

<div class="question">
	<label>Solution
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Il faut utiliser le mode `changevalue.` Nous verrons les autres modes plus tard.

</span>
</label>
</div>
<br>

</div>

<div class="block green-block"><p class="title exercice"></p>

- Renseignez la valeur de **NoData** avec l'application [==ManageNoData==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ManageNoData.html?highlight=data%20type#change-the-no-data-value-options) :
   - soit dans la boîte à outils :  ==ToolBox== > ==OTB== > ==Image Manipulation== >[==ManageNoData==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ManageNoData.html?highlight=data%20type#change-the-no-data-value-options)  ;
   - soit en ligne de commande `otbcli_ManageNoData` :
  ```bash
  otbcli_ManageNoData -in fabas_12_10_2013_V2_reordered.tif -out fabas_12_10_2013_V2_reordered_nodata_ok.tif uint16 -mode changevalue -mode.changevalue.newv 65535.0
  ```

- Ouvrez l'image produite et constatez dans les méta-données la nouvelle valeur de **NoData** (à l'aide de ==ReadImageInfos==)

</div>


## Emprise et masque de NoData

Vous avez constaté qu'une partie au nord et au sud ne contenait pas de données utiles. Vous avez vu plus tôt comment configurer les valeurs de nodata d'une image (**étape 1** sur la figure ci-dessous). Une autre possibilité est de supprimer les parties de l'image qui ne servent à rien (résultat de **l'étape 5**): on ne s'encombre plus des de ces zones pour de futurs traitements et l'image en sortie est moins grande et prend donc moins de place sur le disque.

Avant de pouvoir découper l'image *Pléiades* il faut **créer un masque de découpe vecteur (étape 2-3-4)**:
- créer un masque d'emprise (**étape 2**) avec ==Manage Nodata== de l'OTB ;
- vectoriser ce masque (**étape 3**) avec ==Polygoniser== de GDAL ;
- sélectionner le polygone qui servira de masque (**étape 4**) ;

Vous pourrez ensuite **découper l'image d'origine grâce au masque vecteur** (**étape 5**) avec ==Gdal Warp== de GDAL.

<figure>
	<img class="zoomable" src="img/emprise_et_nodata.png" title="" width="100%">
	<figcaption>Étapes pour supprimer les zones de NoData.</figcaption>
</figure>


Procédons par étape :


<div class="block green-block"><p class="title exercice"></p>


Créez un fichier **vecteur** correspondant à **l'emprise à garder** (**étapes 2 et 3**):
   - Créez un **masque raster binaire des zones à garder** (=1) et des zones à enlever (=0) à l'aide de l'outil [==ManageNoData==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ManageNoData.html?highlight=data%20type#change-the-no-data-value-options). Cette fois-ci, **le mode à utiliser est BuildMask**.
     - soit dans la boîte à outils :  ==ToolBox== > ==OTB== > ==Image Manipulation== >[==ManageNoData==](https://www.orfeo-toolbox.org/CookBook/Applications/app_ManageNoData.html?highlight=data%20type#change-the-no-data-value-options)  ;
     - soit en ligne de commande ==otbcli_ManageNoData==
       - exemple de traitement en ligne de commande :
       ``` bash
       otbcli_ManageNoData -in fabas_12_10_2013_V2_reordered_nodata_ok.tif -out fabas_12_10_2013_V2_extent_mask.tif uint8 -mode buildmask -mode.buildmask.inv 1 -mode.buildmask.outv 0
       ```
   - Vectorisez la partie à garder à l'aide de l'outil GDAL [==gdal_polygonize==](https://gdal.org/programs/gdal_polygonize.html). .
     - soit dans le menu :  ==Raster== > ==Conversion== > [==Polygoniser==](https://docs.qgis.org/3.10/fr/docs/user_manual/processing_algs/gdal/rasterconversion.html#polygonize-raster-to-vector) ;
     - soit en ligne de commande `gdal_polygonize.py`
       - exemple de traitement en ligne de commande :
       ``` bash
       gdal_polygonize.py -mask fabas_12_10_2013_V2_extent_mask.tif fabas_12_10_2013_V2_extent_mask.tif fabas_12_10_2013_V2_extent_mask.shp
       ```
 - Pourquoi n'aurait-ce pas été satisfaisant de faire ce masque à la main ?

 <div class="question">
	<label>Réponse
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Dans notre exemple, il n'y a qu'une seule zone à garder et elle a une géométrie très simple (c'est un rectangle). Vous auriez donc pu créer ce masque à la main en prenant des précautions, mais il y aurait eu de fortes chances de faire des erreurs de délimitation lors de la numérisation de la zone à garder. Il vaut donc mieux passer par une méthode automatique, plus précise et beaucoup moins fastidieuse dans le cas où les zones à extraires sont multiples et avec des géométries plus complexes.


</span>
</label>
</div>

<br>
</div>

<div class="block green-block"><p class="title exercice"></p>

Découpez l'image précédemment réordonnée (**étapes 4 et 5**):
- **Sélectionnez le polygone qui correspond à la zone de l'image à garder** (à l'aide d'une requête attributaire ou manuellement) ;
- Grâce au masque vecteur nouvellement créé, **découper l'image  ré-ordonnée** (`fabas_12_10_2013_V2_reordered.tif` dans mes exemples) :
   - soit dans le menu :  ==Raster== > ==Extraction== > [==Découper un raster selon une couche de masque==](https://docs.qgis.org/3.10/fr/docs/user_manual/processing_algs/gdal/rasterconversion.html#polygonize-raster-to-vector). **Attention à bien cocher la case** ![](img/markdown-img-paste-2021021510451426.png);
   - soit en ligne de commande `gdalwarp`
     - exemple de traitement en ligne de commande :
       ``` bash
       gdalwarp -crop_to_cutline -cutline fabas_12_10_2013_V2_extent_mask.shp fabas_12_10_2013_V2_reordered.tif fabas_12_10_2013_V2_reordered_extent_ok.tif
       ```

- Ouvrez l'image et constatez le résultat à l'aide de l'outil ==ValueTool==

</div>

<div class="block blue-block">
<p class="title"> Astuce n°1</p>

La plupart des fonctionnalités présentes dans le menu ==Raster== de Qgis correpondent à des fonctionnalités GDAL. Vous pourrez donc trouver une documentation détaillée de ces fonctionnalités sur la [doc de GDAL](https://gdal.org/programs/index.html#raster-programs).
</div>

<div class="block blue-block">
<p class="title"> Astuce n°2</p>

Pour celles et ceux qui voudraient lancer gdal en ligne de commande, il est possible d'avoir une idée de la syntaxe en ouvrant l'interface raphique de la fonction correspondante sur QGIS :

<figure>
	<img src="img/correspondance_qgis_gdal.png" title="Correspondance entre l'interface graphique de QGIS et GDAL pour la fonction gdalwarp" width="90%">
	<figcaption>QGIS vous aide pour les lignes de commande.</figcaption>
</figure>

</div>

---------
# Ressources Annexes

- [Aide sur l'utilisation de l'OTB](s8_installation.html#utilisation-de-lotb)
- [Aide sur l'utilisation d'un terminal](aide_lcmd.html)
