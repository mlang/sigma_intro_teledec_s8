---
title: Page d'acceuil
author: Marc Lang
html:
  toc: false
  embeb_local_images: true
  embed_svg: true
  cdn: true
export_on_save:
    html: True
---

<center><img src="img/logo_ensat.png" title="" width="50%"></center>

<center><section style="font-size:300%"> Télédétection </section></center>

<center><section style="font-size:175%; color:#9d1300; width: 65%"> ENSAT S8 2020-2021 </section></center>

<center><section style="font-size:150%"> Marc Lang </section></center>


<p> <br> <br></p>


@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/redish_section.less"
@import "less/link.less"
@import "less/blue_highlight.less"
@import "less/inline_code_red.less"
@import "less/iframe.less"

<section style="font-size:200%"> Contenu </section></center>

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->

  - [Planning](#planning)
  - [Objectifs](#objectifs)
  - [Pré-requis](#pré-requis)
  - [Support de cours](#support-de-cours)
  - [Travaux dirigés](#travaux-dirigés)
  - [Evaluation](#evaluation)
- [Ressources Annexes](#ressources-annexes)
  - [Vidéo de vulgarisations](#vidéo-de-vulgarisations)
    - [Pourquoi les feux rouges sont rouges](#pourquoi-les-feux-rouges-sont-rouges)
    - [La catastrophe ultraviolette](#la-catastrophe-ultraviolette)
    - [Le rose n'existe pas](#le-rose-nexiste-pas)

<!-- /code_chunk_output -->



## Planning

Les dates en gras sont les dates en présentiel.

<div class="my_table">

| Date                                 | Durée |                      Sujet                      |
|:-------------------------------------|:-----:|:-----------------------------------------------:|
| **Ma 06 avril 2021 - 8h00 - 12h20**  |  4h   |        Cours - Visualisation des images         |
| Ma 06 avril 2021 - 14h00 - 17h00     |  3h   |            Pré-traitement des images            |
| Mer 07 avril 2021 - 9h00 - 12h00     |  3h   | Calcul d'indices - Classification par seuillage |
| Mer 07 avril 2021 - 14h00 - 17h00    |  3h   |            Détection des inondations            |
| **Ve 09 avril 2021 - 8h00 - 12h20**  |  4h   |              Classification (1/2)               |
| Ve 09 avril 2021 - 14h00 - 17h00     |  3h   |              Classification (2/2)               |
| Ma 13 avril 2021 - 9h00 - 12h00      |  3h   |                    Révision                     |
| **Ma 13 avril 2021 - 13h30 - 17h50** |  2h   |                     Examen                      |

</div>


## Objectifs

L’objectif princpal de ces séances est d'*être capable d’utiliser et d’extraire des informations à partir d’images de télédétection pour l’aménagement du territoire ou la gestion des milieux naturels.*

Nous détaillerons au fur et à mesure les compétences à acquérir à l'issue de nos séances.  


## Pré-requis

- [Installation de l'OTB](s8_installation.html)

## Support de cours

Vous pouvez télécharger le support de cours [ici](https://filesender.renater.fr/?s=download&token=f9bc5150-c73d-48ad-87b4-dee878e6a7e1).

## Travaux dirigés

- [Séance n°1](s8_labworks_1.html)
- [Séance n°2](s8_labworks_2.html)
- [Séance n°3](s8_labworks_3.html)
- [Séance n°4](s8_labworks_4.html)
- [Séance n°5](s8_labworks_5.html)

## Evaluation

- [données](https://filesender.renater.fr/?s=download&token=81a27df6-49dc-4636-8f0e-862e634e2f3a)


-------------

# Ressources Annexes

- [Aide sur l'utilisation de l'OTB](s8_installation.html#utilisation-de-lotb)
- [Aide sur l'utilisation d'un terminal](aide_lcmd.html)

## Vidéo de vulgarisations

Vous trouverez ici quelques vidéos de vulgarisation scientifique que vous pouvez visionner si vous êtes curieu.ses.x. Elles approfondissent ou résument des concepts vus en cours.

### Pourquoi les feux rouges sont rouges

Une vidéo d'[e-penser](https://www.youtube.com/channel/UCcziTK2NKeWtWQ6kB5tmQ8Q), vulgarisateur de concepts relatifs au domaine de la physique/chimie qu'on ne présente plus. Cette vidéo vous présentera pourquoi le spectre du visible est le spectre du visible. Pourquoi voit on du bleu au rouge et pas du moyen infrarouge au radar ? La réponse ici :  


<iframe width="1280" height="720" src="https://www.youtube.com/embed/LsQIWrzb1jo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### La catastrophe ultraviolette

Une autre vidéo d'[e-penser](https://www.youtube.com/channel/UCcziTK2NKeWtWQ6kB5tmQ8Q).

Accrochez vous, on parle ici de spectre de rayonnement des corps noirs, de comment on trouve l'équation qui permet de la modéliser ? Pourquoi cherche-t-on à la modéliser ? Quelle est la théorie derrière ? D'autres concepts sur le rayonnement électromagnétiques sont également abordés.

<iframe width="1280" height="720" src="https://www.youtube.com/embed/nxHH-uq_0Sg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Le rose n'existe pas

Une vidéo de [dirty biology](https://www.youtube.com/channel/UCtqICqGbPSbTN09K1_7VZ3Q), vulgarisateur qui s'intéresse originellement à des sujets sur la bioloie et l'écologie évolutive mais qui touche aussi à de nombreux autres sujets. Cette vidéo traite, entre autres, de la relation entre les longueurs d'ondes et les couleurs qui y sont associées et pose la question de ce que ça raconte sur la nauture de la réalité.  

<iframe width="1280" height="720" src="https://www.youtube.com/embed/Wdy4YBULvdo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
