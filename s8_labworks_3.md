---
title: Télédétection - TD3
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer2.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/image_zoom.less"
@import "less/iframe.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/blocks.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection  </section></center>
<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD 3 :  Calcul d'indices spectraux - classification par seuillage</section></center>
<br>
<center><section style="font-size:150%"> Marc Lang </section></center>
<p> </p>

<iframe src="https://cdn.knightlab.com/libs/juxtapose/latest/embed/index.html?uid=0eb4562a-1f98-11ea-b9b8-0edaf8f81e27" scrolling="yes" width="100%" height="500px" frameborder="0"></iframe>

<div class="question">
	<label> Curiosité n°3 : D'après vous, de quoi s'agit-il ?
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  Cette animation est issue d'un article de Simon Gascoin dans le blog multitemp du laboratoire du CESBIO. Vous retrouverez tous les détails [ici](https://labo.obs-mip.fr/multitemp/les-inondations-dans-le-sud-ouest-vues-par-satellite/).

  Pour résumé : cette animation permet de visualiser l'impact de la crue de la Garonne dans le Sud-Ouest de la France en 2019. Les images ont été prises avec le Satelitte Sentinel 2.

</span>
</label>
<br><br>
</div>

<section style="font-size:200%"> Contenu </section></center>



<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=4 orderedList=False} -->
<!-- code_chunk_output -->

  - [Indice de végétation](#indice-de-végétation)
  - [Seuillage d'image (classification experte)](#seuillage-dimage-classification-experte)
    - [Analyse spectrale 1D](#analyse-spectrale-1d)
    - [Seuillage 1D](#seuillage-1d)
      - [Syntaxe de la calculatrice raster](#syntaxe-de-la-calculatrice-raster)
      - [Syntaxe de BandMath](#syntaxe-de-bandmath)
      - [Mise en pratique](#mise-en-pratique)
    - [Analyse spectrale 2D](#analyse-spectrale-2d)
    - [Seuillage du Scatter splot](#seuillage-du-scatter-splot)
- [Ressources Annexes](#ressources-annexes)

<!-- /code_chunk_output -->

## Indice de végétation

Parmi les indices radiométriques existants, seul le NDVI sera utilisé dans ce TD car il est l'indice de végétation le plus utilisé en télédétection.

Pour rappel la formule du NDVI est la suivante :

$$NDVI = \frac{IR - R}{IR + R}$$

Il est possible de calculer le NDVI de plusieurs manières. Soit :

- avec la [==calculatrice raster==](https://docs.qgis.org/3.10/fr/docs/user_manual/working_with_raster/raster_analysis.html#raster-calculator) ;
- avec l'application OTB [==BandMath==](https://www.orfeo-toolbox.org/CookBook/Applications/app_BandMath.html?highlight=bandmath), dans QGIS, avec Mapla ou en ligne de commande ;
- avec l'application OTB [==RadiometricIndices==](https://www.orfeo-toolbox.org/CookBook/Applications/app_RadiometricIndices.html), dans QGIS, avec Mapla ou en ligne de commande.  

Les deux premiers outils permettent de faire des opérations entre les bandes des différentes images que vous avez chargées dans QGIS. On peut les utiliser pour calculer le NDVI mais également pour d'autres calculs.  En revanche, les deux outils suivent une syntaxe qui n'est pas la même (ce serait trop facile). Pour la ==calculatrice raster== vous pouvez vous référer à [cette doc](https://docs.qgis.org/3.10/fr/docs/user_manual/working_with_raster/raster_analysis.html#raster-calculator) et pour la syntaxe de ==BandMath== à [cette doc](https://www.orfeo-toolbox.org/CookBook/Applications/app_BandMath.html?highlight=bandmath).

La ==Calculatrice Raster== est disponible depuis le menu ==Raster== > ==Calculatrice Raster==.

<figure>
	<img src="img/markdown-img-paste-20210216150818291.png" title="" width="75%">
	<figcaption>Outil Calculatrice Raster</figcaption>
</figure>

Pour la ==calculatrice raster== entrez la formule suivante pour le calcul du NDVI (dans la partie *expression de la calculatrice raster* sur la figure ci-dessus):

```
("fabas_12_10_2013@4"-"fabas_12_10_2013@1")/("fabas_12_10_2013@4"+"fabas_12_10_2013@1")
```

L'application ==BandMath== est quant à elle disponible depuis la ==Boîte à outils de traitements== > ==OTB== > ==Image Manipulation== > ==BandMath==.

<figure>
	<img src="img/markdown-img-paste-20210216151314837.png" title="" width="75%">
	<figcaption>Outil BandMath</figcaption>
</figure>

L'expression à rentrer pour le calcul du NDVI (dans la partie *expression* sur la figure ci-dessus) est  :

```
"(im1b4-im1b1)/(im1b4+im1b1)"
```

Voici également un exemple de syntaxe de pour l'application OTB ==BandMath== en ligne de commande :
```
otbcli_BandMath -il fabas_12_10_2013.tif -out ndvi_fabas.tif -exp"(im1b4-im1b1)/(im1b4+im1b1)"
```

Enfin, voici un exemple de syntaxe de pour l'application OTB ==RadiometricIndices== en ligne de commande :
```
otbcli_RadiometricIndices -il fabas_12_10_2013.tif -list Vegetation:NDVI -channels.red 1 -channels.nir 4  -out ndvi_fabas.tif
```

<div class="block blue-block">
<p class="title"> ASTUCE</p>

De manière générale, n'hésitez pas à consultez la documentation des applications que vous utilisez pour savoir comment remplir certains champs.

Vous pouvez obtenir de l'aide en cliquant sur l'icone d'aide ![](img/markdown-img-paste-20210215135832232.png) en bas des fenêtres des applications.
</div>

<div class="block green-block"><p class="title exercice"></p>

Vous êtes libres d'utiliser l'outil de votre choix.

- Calculez le NDVI ;
  - Pour le 12 octobre ;
  - pour le 10 décembre ;
- Comparez le NDVI de chaque date (à l'aide de ==MapSwipeTool== par exemple) et expliquez ce que vous voyez.
- Testez les deux autres outils sur une seule date cette fois-ci.
</div>

<div class="block red-block">
<p class="title"> Erreur Courante</p>

<div style="display: flex; align-items: center;">

<img src="img/shadock_image.png" title="" width="80%" height="100%" style="float:left; margin-right: 10px;">

<div syle="width:39%">

Vous allez faire des erreurs, c'est normal. Ce serait de ne pas en faire qui n'est pas normal. Certaines peuvent être dues à une mauvaise utilisation des paramètres d'une application, il faut dans ce cas là se référer à l'aide des applications. D'autres erreurs sont dues à de  l'inattention, parce que vous êtes allé.e.s trop vite.

Dans tous les cas, prenez le temps de regardez les messages d'erreur. Ils ne sont pas toujours faciles à comprendre mais ils vous aideront souvent (pas toujours malheureusement) à trouver la source de votre erreur (voir exemple sur la figure ci-dessous)

</div>
</div>
<br>

<figure>
	<img src="img/erreur_ndvi.png" title="" width="100%">
	<figcaption>Exemple d'une erreur de frappe lors de l'utilisation de l'application RadiometricIndices (l'erreur est surlignée dans la partie de droite).</figcaption>
</figure>


</div>


## Seuillage d'image (classification experte)


Dans ce TD, vous allez réaliser votre première classification : vous allez attribuer à chaque pixel d'une image un label qui correspondra à une occupation du sol. Aujourd'hui, nous nous focaliserons sur une approche non supervisée et manuelle : vous allez vous même déterminer les règles permettant de discrimer les labels de chaque pixel.

En fonction de votre interlocuteur, le terme utilisé pourra varier. Certains parleront de **segmentation (simple) d'images**, d'autres de **classification experte** (au sens d'expertisée par une personne) ou encore de **seuillage**.

**De quelle occupation du sol parle-t-on ?**

Dans ce TD nous ne connaissons pas _a priori_ les occupations du sol qu'il est possible de discriminer. Vous allez analyser les dynamiques spectrales des images, aussi bien des bandes natives (Rouge, Vert, Bleu et Proche Infrarouge) que de l'indice NDVI. Vous verrez dans un premier temps comment analyser la dynamique spectrale d'une bande (ou d'un indice) puis dans un second temps comment analyser la dynamique de deux bandes en même temps.

### Analyse spectrale 1D

Dans le meilleur des mondes, les occupations du sol principales présentes dans une image peuvent être identifées sur l'histogramme d'une bande. Dans l'histogramme théorique ci dessous, les classes de sol nu (Sn), Herbe (H), Ligneux bas (LB) et Ligneux Hauts (LH) se distinguent par des *modes* différents (ou des pics). Dans ce cas là on peut imaginer différencier ces classes grâce à des seuils entre les modes.

<figure>
	<img src="img/markdown-img-paste-20210215135006384.png" title="" width="100%">
	<figcaption>Exemple d'histogramme de NDVI théorique</figcaption>
</figure>

Aujourd'hui, les classes d'occupation du sol que vous allez classifiez ne correspondront pas forcément à du sol nu, de l'herbe, des ligneux bas ou des ligneux hauts. À vous de le découvrir !

Vous pouvez visualiser les Histogrammes d'une bande dans QGIS en : ==Clique Droit== sur la couche votre choix > ==Propriétés== > ==Histogramme== :

<figure>
	<img src="img/markdown-img-paste-20210215144206336.png" title="" width="80%">
	<figcaption>Histogramme du NDVI de l'image du 10 décembre.</figcaption>
</figure>



**Remarque** : les histogrammes peuvent parfois être plus difficiles à lire selon les machines avec lesquelles vous travaillez.

<div class="block green-block"><p class="title exercice"></p>

Dans cette partie vous allez afficher l'histogramme du NDVI d'octobre et essayer d'identifier à quelle occupation du sol correspond les modes que vous observez :

- Regardez l'[==Histogramme==](https://docs.qgis.org/3.10/fr/docs/user_manual/working_with_raster/raster_properties.html#histogram-properties) et identifiez les maximums locaux ;
- Notez les caractéristiques de chaque mode (position centrale et largeur).
- Par chaque mode, essayez de déterminer dans l'image à quels pixels ils correspondent (à l'aide de ==Value Tool==, ou en réglant le contraste de l'image).

</div>

### Seuillage 1D

Ici, l'objectif est de regrouper les pixels ayant des caractéristiques spectrales similaires. L'analyse de l'histogramme que vous avez faite dans la partie précédente vous a permis d'identifier plusieurs classes spectrales. Vous allez maintenant **attribuer un label à chacun des pixels** grâce à l'aide de l'application ==BandMath== ou de la ==calculatrice raster==.

Le principe est d'attribuer un label (par exemple 2) à  des pixels ayant une gamme de valeurs que vous aurez déterminée (par exemple les pixels compris entre -0.2 et 0). Selon l'application choisie la syntaxe n'est pas la même.

#### Syntaxe de la calculatrice raster


Pour sélectionner les pixels compris entre -0.2 et 0 (c'est un exemple) la syntaxe à utiliser (dans le cadre *expression de la calculatrice raster* sur la figure ci-dessus) est la suivante :

```
("fabas_10_12_2013_ndvi@1" > -0.2) AND ("fabas_10_12_2013_ndvi@1" < 0)
```

Pour la attribuer la valeur de `2` la syntaxe à utiliser est la suivante :

```
(("fabas_10_12_2013_ndvi@1" > -0.2) AND ("fabas_10_12_2013_ndvi@1" < 0)) * 2
```

#### Syntaxe de BandMath


Dans la [documentation](https://www.orfeo-toolbox.org/CookBook/Applications/app_BandMath.html?highlight=bandmath) on peut lire que la syntaxe de conditions est la suivante :

 `(condition ? value_true : value_false)`

Si on souhaite attribuer une valeur de 2 à tous les pixels dont la valeur de NDVI est inférieure à 0 :
- condition : `im1b1 < 0` ;
- résultat si vrai : `2` ;
- résultat si faux : `0`.

et la formule devient : `im1b1 < 0 ? 2 : 0`.

Il est également possible de faire des conditions imbriquées pour attribuer une valeur de 2 aux pixels compris entre -0.2 et 0 :

`im1b1 > 0 ? 0 : (im1b1 > -0.2 ? 2 : 0)`

On peut traduire par :

- **si** (NDVI > 0) :
   - **alors** résulat <-- 0 :
- **sinon** (NDVI <= 0) :
  - **si** (NDVI > -0.2) :
    - **alors** (résulat <-- 2)
  - **sinon** (NDVI <= -0.2):
    - **alors** (résulat <-- 0)

**Remarque :** l'avantage de ==BandMath== par rapport à la ==calculatrice raster== est que vous pouvez définir le type de l'image en sortie (par exemple en entier 8 bits)

#### Mise en pratique


<div class="block green-block"><p class="title exercice"></p>

À l'aide de l'application ==BandMath== ou la ==calculatrice raster==, seuillez votre image :

- pour obtenir une image (une classification) **à une classe** pour commencer ;
- pour obtenir une classification avec **autant de classes que de modes identifiés précédemment** ;
- quel est le type de l'image en sortie qu'il faudrait renseigner (si possible) ?
<div class="question">
	<label>Réponse
	<input type="checkbox">
	<i class="arrow"></i> <span class="reponse">

  Les labels que vous allez attribuer aux classes identifiées sur l'histogramme sont des entiers positifs. Vous pouvez donc utiliser le type `uint8`. Si vous utilisez un autre type, ce n'est pas grave, mais l'image en sortie prendra plus de place que nécessaire.

</span></label>

</div>
<br>
</div>



### Analyse spectrale 2D

Pour cette partie, il faut installer le plugin [RasterDataPlotting](https://raster-data-plotting.readthedocs.io/en/latest/index.html).

<figure>
	<img src="img/droite_sol.png" title="Répartition spectrale des réflectances dans le rouge et le proche infrarouge" width="100%">
	<figcaption>Répartition spectrale des réflectances dans le rouge et le proche infrarouge</figcaption>
</figure>


L'objectif est de repérer des classes spectrales à partir des bandes **Rouge** et **Infrarouge**. On parle de Scatter Plot 2D. Cette représentation est très utile pour distinguer les différents types de
végétation, de sol et d’enneigement (voir figure ci-dessus).

Vous pouvez obtenir cette représentation en configurant le pluggin ==RasterDataPlotting== comme suit

<figure>
	<img src="img/scatter_plot_paremeter.png" title="" width="80%">
	<figcaption> Configuration du pluggin RasterDataPlotting pour obtenir un scatter Plot 2D representant la bande Rouge et Infra-rouge.</figcaption>
</figure>


<div class="block green-block"><p class="title exercice"></p>

- Affichez le Scatter Plot 2D de l'image `fabas_10_12_2013.tif` avec la bande Rouge en abscisse et la bande InfraRouge en ordonnée.
- Combien de classes spectrales distinguez-vous ?
</div>

**Comment faire pour faire le lien entre le scatter plot et les pixels de l'image ?**

Deux fonctionnalités peuvent vous aider à faire le lien entre les classes spectrales que vous distinguées sur le scatter plot et les occupations du sol. La première est le fait que seul les pixels contenus dans l'emprise de votre Canevas QGIS sont affichés dans dans le scatter plot : pour voir le comportement spectrale d'une occupation du sol, zoomez sur celle-ci comme sur l'exemple ci-dessous.

<figure>
	<img src="img/Raster_Datta_Plot_1.gif" title="" width="95%">
	<figcaption>Le Scatter Plot s'adapte à l'emprise de votre Canevas.</figcaption>
</figure>

De manière inverse vous pouvez mettre en évidence sur le Canevas les pixels correspondant à une classe spectrale en utilisant le bouton ==Spectral ROI== ![](img/markdown-img-paste-20210216155437283.png) comme sur l'exemple ci-dessous. Attention toutefois, l'utilisation des ==Spectral ROI== demande un peu de doigté et sera certainement source de frustration au bout de quelques essais infructueux. Prenez patience ! cet outil en vaut la peine.

<figure>
	<img src="img/Raster_Datta_Plot_2.gif" title="" width="95%">
	<figcaption>Exemple de mise en évidence de classes spectrales.</figcaption>
</figure>


<div class="block green-block"><p class="title exercice"></p>

1. À l'aide de ==RasterDataPlotting==, identifiez à quelles occupations du sol correspondent les classes spectrales que vous avez identifiées dans l'exercice précédent.
2. Comme pour les modes des histogrammes 1D, notez les caractéristiques de chaque classe spectrale. Vous identifierez cette fois des équations de droite qui séparent deux classes, ou des bornes inférieurs et supérieurs de chaque bandes.
</div>

<div class="block blue-block">
<p class="title"> Astuce : équation d'une droite </p>

À partir de deux points (identifiés visuellement par exemple) vous pouvez déterminer l'équation d'une droite. Ça remonte à loin tout ça, je sais je sais ...

<div class="question">
	<label>Trop loin ?
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  <div style="display: flex; align-items: center;">

  <img src="https://media.giphy.com/media/gEvab1ilmJjA82FaSV/giphy.gif" title="" width="25%">
  <img src="img/equation_droite.png" title="" width="69%">
  </div>

</span>
</label>
</div>
<br>

</div>


### Seuillage du Scatter splot

Ici, l'objectif est encore et toujours de regrouper les pixels ayant des caractéristiques spectrales similaires. L'analyse du scatter plot que vous avez faite dans la partie précédente vous a permis d'identifier plusieurs classes spectrales. Vous allez maintenant attribuer un label à chacun des pixels.

<figure>
	<img src="img/equation_droite_regle.png" title="" width="55%">
	<figcaption>Deux classes spectrales séparables par une droite dans le plan spectral IR / R</figcaption>
</figure>

La disctinction de deux classes (2 et 1 par exemple) séparées par une droite dessinée dans le plan spectral constitué par la bande Infra rouge (bande 4) et la bande Rouge (bande 1) comme sur la figure ci-dessus, peut se faire comme suit :

```
"im1b4 > (a * im1b1 + b) : 2 ? 1"
```

<div class="block green-block"><p class="title exercice"></p>

1. À l'aide de l'application [==BandMath==](https://www.orfeo-toolbox.org/CookBook/Applications/app_BandMath.html?highlight=bandmath) ou la ==calculatrice raster==, seuillez votre image et enregistrez votre seuillage finale (par ex: `fabas_10_12_2013_seuil.tif`). Ne supprimez pas l'image à la fin du TD.
2. Quel type de d'image en sortie faut il renseigner ?
3. Changez éventuellement le mode de représentation de l'image : ==Clique droit== > ==propriétés== > ==syle de couche== > [==Palette /Uniques==](https://docs.qgis.org/3.10/fr/docs/user_manual/working_with_raster/raster_properties.html#symbology-properties)) pour que le résultat de votre seuillage soit plus lisible :

<figure>
	<img src="img/markdown-img-paste-20210216172835557.png" title="" width="70%">
	<figcaption>Exemple de sémiologie à obtenir pour votre classification.</figcaption>
</figure>


4. Les classes issues vos seuillages sont-elles conformes à ce que vous aviez repéré précédemment ?


</div>

---------

# Ressources Annexes

- [Aide sur l'utilisation de l'OTB](s8_installation.html#utilisation-de-lotb)
- [Aide sur l'utilisation d'un terminal](aide_lcmd.html)
