---
title: Télédétection - Ligne de commande
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer.less"
@import "less/blocks.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/image_zoom.less"
@import "less/link.less"
@import "less/font.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>

<center><section style="font-size:300%"> Télédétection  </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> Utilisation d'applications en ligne de commande </section></center>

<br>
<center><section style="font-size:150%"> Marc Lang </section></center>

<p> </p>


## Ouvrir un terminal

**Sous linux**

Tapez ==CTRL== + ==Alt== + ==T==.

**Sous windows**

==Menu démarrer== > ==Anaconda Prompt== (attention ce n'est pas le prompt PowerShell qu'il faut lancer)

**Sous Mac OS X**

Tapez "terminal" dans le menu de recherche d'applications (Spotlight) sous MacOS X (voir [ici](https://fr.wikihow.com/ouvrir-le-Terminal-sur-un-Mac) si vous ne trouvez pas le terminal)

## Commandes basiques dans un terminal Linux et MacOS X

- `cd` : Change de répertoire. Pour entrer dans un répertoire : `cd Nom/du/repertoire` ; Pour revenir au niveau supérieur : `cd ..`

- `ls` : Liste tous les fichiers et les dossiers présents dans le répertoire courant ;

- `pwd` : Retourne le chemin complet du répertoire courant ;

- `cp` : Copie un fichier ou un répertoire, par ex. : `cp repertoire1/mon_fichier.txt repertoire_destination/copie_mon_fichier.txt` ;

- `mv` : Déplace un fichier ou un répertoire, par ex. : `mv repertoire1/mon_fichier.txt repertoire_destination/copie_mon_fichier.txt` ;

- `mkdir` : crée un nouveau répertoire, par ex. : `mkdir nouveau/repertoire` ;

- `touch` : crée un nouveau fichier texte, par ex : `touch mes_textes/nouveau_fichier_texte.txt` ;

## Commandes basiques dans un prompt windows

- `cd` : Affiche le répertoire actuel et vous permet de passer à d’autres répertoires. Avec le paramètre /D plus le disque dur et le chemin d’accès, vous pouvez également modifier le disque dur. Avec `cd.` vous passez au répertoire supérieur.

- `dir` : Affiche tous les dossiers et fichiers du répertoire courant. Vous pouvez limiter l’édition par attributs (/A), simplifier la liste (/B), ou afficher tous les sous-répertoires et leurs fichiers (/S).

- `mkdir` : Crée un nouveau répertoire au chemin spécifié. Si les répertoires n’existent pas déjà sur le chemin, mkdir les crée automatiquement. (la commande md peut également être utilisée.)

- `copy` : Copie un fichier ou un répertoire, par ex. : `copy C:\repertoire1\mon_fichier_text.txt F:\sauvegardes-donnees` ;

- `move` : Déplace un fichier ou un répertoire, par ex. : `mv C:\repertoire1\mon_fichier_text.txt F:\sauvegardes-donnees\` ;

## Lancer des applications OTB

### Configuration du terminal

L'objectif est de faire en sorte que les applications OTB soient accessibles depuis votre terminal car elles ne le sont pas par défaut. Pour cela, il faut activer un fichier de configuration de l'OTB.

Il faudra faire cette opération à chaque fois que vous ouvrez un terminal.

#### Sous Linux et MacOS

1. ouvrir un terminal (==CTRL + ALT + T==)
2. se déplacer dans le dossier d'installation  `cd /chemin/repertoire/`
3. exécuter le fichier `otbenv.profile` : `source otbenv.profile`

```bash
# déplacement (le chemin d'accès est un exemple)
cd /usr/local/OTB-7.1.0-Linux64

# Voir les fichiers présents
ls

# Exécuter le fichier de configuration
source otbenv.profile
```

Vous pouvez maintenant accéder aux applications `otbcli`, `otbgui` et aux applications ==GDAL==

#### Sous windows


1. Ouvrir un terminal : ==Menu démarrer== > ==Anaconda Prompt== (attention ce n'est pas le prompt PowerShell qu'il faut lancer)
2. se déplacer dans le dossier d'installation  `cd \chemin\repertoire\`
3. executer le fichier `otbenv.bat` : `otbenv.bat`

```bash
# déplacement (le chemin d'accès est un exemple)
cd `C:\OTB-7.2.0-Win64`

# Voir les fichiers présents
dir

# Exécuter le fichier de configuration
otbenv.bat
```

Vous pouvez maintenant accéder aux applications `otbcli`, `otbgui` et aux applications ==GDAL==

### Lancer les applications depuis un terminal

Une fois votre terminal configuré, vous pouvez lancer les applications en ligne de commande (`otbcli_NomDeLapplication`).

**Exemple** : Obtenir des informations sur une image avec l'application ==ReadImageInfo==.

Si vous entrez le nom de l'application, sans paramètres :
```bash
otbcli_ReadImageInfo
```

vous obtiendrez un message d'erreur avec une aide sur l'utilisation de l'application :
```bash
ERROR: Waiting for at least one parameter.


This is the ReadImageInfo application, version 6.7.0

Get information about the image
Complete documentation: https://www.orfeo-toolbox.org/CookBook/Applications/app_ReadImageInfo.html or -help

Parameters:
MISSING -in          <string>         Input Image  (mandatory)
        -keywordlist <boolean>        Display the OSSIM keywordlist  (mandatory, default value is false)
        -outkwl      <string>         Write the OSSIM keywordlist to a geom file  (optional, off by default)
        -rgb         <group>          Default RGB Display
        -progress    <boolean>        Report progress
        -help        <string list>    Display long help (empty list), or help for given parameters keys

Use -help param1 [... paramN] to see detailed documentation of those parameters.

Examples:
otbcli_ReadImageInfo -in QB_Toulouse_Ortho_XS.tif
```

**Remarque** : [la documentation de l'OTB](https://www.orfeo-toolbox.org/CookBook/) vous fournira une aide détaillée des paramètres à renseigner, en plus des indications fournies dans le terminal.

Il fallait ici renseigner le chemin de l'image pour laquelle on souhaite obtenir des informations à l'aide du paramètre `-in` :

```bash
otbcli_ReadImageInfo -in /home/terudel/Documents/cours/2020-2021/ENSAT/2A_S8/data/Pleiades/fabas_10_12_2013.tif
```

et on obtiendra en sortie quelque chose dans ce gout là :

```bash
2021-04-01 10:29:53 (INFO) ReadImageInfo: Default RAM limit for OTB is 256 MB
2021-04-01 10:29:53 (INFO) ReadImageInfo: GDAL maximum cache size is 1596 MB
2021-04-01 10:29:53 (INFO) ReadImageInfo: OTB will use at most 12 threads
2021-04-01 10:29:53 (INFO) ReadImageInfo:
Image general information:
	Number of bands : 4
	Data type : unsigned_short
	No data flags : Not found
	Start index :  [0,0]
	Size :  [3390,2074]
	Origin :  [522844,6.24866e+06]
	Spacing :  [2,-2]
	Estimated ground spacing (in meters): [1.99341,2.00065]

Image acquisition information:
	Sensor :
	Image identification number:
	Image projection : PROJCS["2154 RGF93 / Lambert93 (FR.)",
    GEOGCS["RGF93 (FR.) [4171]",
        DATUM["unknown",
            SPHEROID["GRS 1980",6378137,298.2572221010042,
                AUTHORITY["EPSG","7019"]]],
        PRIMEM["Greenwich",0],
        UNIT[,0.0174532925199433]],
    PROJECTION["Lambert_Conformal_Conic_2SP"],
    PARAMETER["standard_parallel_1",44],
    PARAMETER["standard_parallel_2",49],
    PARAMETER["latitude_of_origin",46.5],
    PARAMETER["central_meridian",3],
    PARAMETER["false_easting",700000],
    PARAMETER["false_northing",6600000],
    UNIT["metre",1,
        AUTHORITY["EPSG","9001"]]]

Image default RGB composition:
	[R, G, B] = [0,1,2]

Ground control points information:
	Number of GCPs = 0
	GCPs projection =

Output parameters value:
indexx: 0
indexy: 0
sizex: 3390
sizey: 2074
spacingx: 2
spacingy: -2
originx: 522844
originy: 6248662
estimatedgroundspacingx: 1.993409157
estimatedgroundspacingy: 2.000645876
numberbands: 4
datatype: unsigned_short
sensor:
id:
time:
town:
country:
rgb.r: 0
rgb.g: 1
rgb.b: 2
projectionref: PROJCS["2154 RGF93 / Lambert93 (FR.)",
    GEOGCS["RGF93 (FR.) [4171]",
        DATUM["unknown",
            SPHEROID["GRS 1980",6378137,298.2572221010042,
                AUTHORITY["EPSG","7019"]]],
        PRIMEM["Greenwich",0],
        UNIT[,0.0174532925199433]],
    PROJECTION["Lambert_Conformal_Conic_2SP"],
    PARAMETER["standard_parallel_1",44],
    PARAMETER["standard_parallel_2",49],
    PARAMETER["latitude_of_origin",46.5],
    PARAMETER["central_meridian",3],
    PARAMETER["false_easting",700000],
    PARAMETER["false_northing",6600000],
    UNIT["metre",1,
        AUTHORITY["EPSG","9001"]]]
keyword:
gcp.count: 0
gcp.proj:
gcp.ids:
gcp.info:
gcp.imcoord:
gcp.geocoord:
```
